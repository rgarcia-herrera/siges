#!/usr/bin/env python

import glob
import random
from shutil import copyfile


plecas_dir = '/var/www/static/assets/plecas/'

plecas = glob.glob(plecas_dir + '*.jpg')

chosen = random.choice(plecas)
while 'default.jpg' in chosen:
    chosen = random.choice(plecas)
    
copyfile(chosen, plecas_dir + 'default.jpg')
