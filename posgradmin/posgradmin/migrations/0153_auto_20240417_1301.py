# Generated by Django 3.2.15 on 2024-04-17 19:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posgradmin', '0152_alter_proyecto_titulo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='curso',
            name='intersemestral',
            field=models.BooleanField(default=False, help_text='marque si solicita impartir el curso en el intersemestre'),
        ),
        migrations.AlterField(
            model_name='miembrojuradograduacion',
            name='rol_asignado',
            field=models.CharField(choices=[('presidente', 'presidente'), ('secretario', 'secretario'), ('vocal 1', 'vocal 1'), ('vocal', 'vocal')], default='D', max_length=25),
        ),
    ]
