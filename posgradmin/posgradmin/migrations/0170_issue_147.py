# Generated by Django 5.1.3 on 2024-12-03 16:36

from django.db import migrations


def forwards_func(apps, schema_editor):

    Historial = apps.get_model("posgradmin", "Historial")
    EntidadPCS = apps.get_model("posgradmin", "EntidadPCS")
    
    db_alias = schema_editor.connection.alias

    for h in Historial.objects.using(db_alias).all():
        if h.institucion:
            h.entidad_pcs = EntidadPCS.objects.using(db_alias).get(pk=h.institucion.id)
            h.save()


class Migration(migrations.Migration):

    dependencies = [
        ('posgradmin', '0169_historial_entidad_pcs_and_more'),
    ]

    operations = [
        migrations.RunPython(forwards_func)        
    ]
