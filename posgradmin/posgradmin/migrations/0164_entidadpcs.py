# Generated by Django 3.2.15 on 2024-11-22 16:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posgradmin', '0163_auto_20241025_1023'),
    ]

    operations = [
        migrations.CreateModel(
            name='EntidadPCS',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=150, verbose_name='Institución u Organización')),
                ('suborganizacion', models.CharField(max_length=150, verbose_name='Dependencia, Entidad o Departamento')),
                ('pais', models.CharField(blank=True, max_length=100, verbose_name='País')),
                ('estado', models.CharField(blank=True, max_length=100)),
            ],
            options={
                'verbose_name_plural': 'entidades pcs',
                'ordering': ['nombre', 'suborganizacion'],
            },
        ),
    ]
