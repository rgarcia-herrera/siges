# Generated by Django 3.2.15 on 2024-03-21 21:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posgradmin', '0150_auto_20240215_1628'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='invitado',
            options={'verbose_name': 'Asesor(a) Externo(a)', 'verbose_name_plural': 'Asesores externos'},
        ),
        migrations.AddField(
            model_name='estudiante',
            name='opcion_graduacion',
            field=models.BooleanField(default=False, verbose_name='Opción a graduación'),
        ),
    ]
