# Generated by Django 3.2.15 on 2024-06-07 03:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posgradmin', '0154_auto_20240527_2016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='estudiante',
            name='estado',
            field=models.CharField(blank=True, choices=[('inscrito', 'inscrito'), ('movilidad', 'movilidad'), ('egresado', 'egresado'), ('graduado', 'graduado'), ('indeterminado', 'indeterminado'), ('baja', 'baja'), ('suspensión 1 sem', 'suspensión 1 sem'), ('suspensión 2 sem', 'suspensión 2 sem'), ('prórroga', 'prórroga')], max_length=25, null=True),
        ),
    ]
