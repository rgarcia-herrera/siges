# Generated by Django 4.2.16 on 2025-01-18 20:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('posgradmin', '0177_resumencurricular_academico'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resumencurricular',
            name='academico',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='resumen_curricular', to='posgradmin.academico'),
        ),
    ]
