from django.contrib import admin
from posgradmin import models
from datetime import datetime


def conteo_inscripciones(e, plan):
    """ cuantas inscripciones tiene un estudiante en un plan """
    i = 0
    for h in e.get_historial(plan):
        if h.estado.startswith('inscrito'):
            i += 1
    return i



@admin.action(description="¿son egresados?")
def marca_egresados(modeladmin, request, queryset):
    # ultimo año en la tabla de historial
    year = models.Historial.objects.order_by('year').last().year

    excluir_ultimo_sem = [h.estudiante
                          for h in models.Historial.objects.filter(estado__startswith="inscrito",
                                                                   year=year,
                                                                   semestre=1)]

    for e in queryset:
        if e in excluir_ultimo_sem:
            continue

        for plan in ['Doctorado', 'Maestría']:

            estados = [h.estado for h in e.get_historial(plan)]

            if not estados:
                # sin estados indica que nunca ha estado en este plan
                continue

            if ('baja_' + plan.lower() in estados
                or 'graduado_' + plan.lower() in estados
                or 'movilidad' in estados
                or 'egresado_' + plan.lower() in estados
                or 'graduado_anticipado_' + plan.lower() in estados
                or 'votos' in estados
                or 'anómalo' in estados):
                continue

            # si el estado actual es indeterminado no puede egresar,
            # indeterminado es una anomalía
            if e.get_historial(plan).first().estado == 'indeterminado':
                continue

            if ((plan == 'Maestría'
                 and conteo_inscripciones(e, plan) == 4)
                or
                (plan == 'Doctorado'
                 and conteo_inscripciones(e, plan) == 8)
                and
                e.get_historial(plan).first().estado.startswith('inscrito')
                ):
                # egresado
                ue = e.get_historial(plan).first()
                h = models.Historial(estudiante=e,
                                     year=ue.year+1,
                                     semestre=1,
                                     estado='egresado_' + plan.lower(),
                                     plan=plan,
                                     institucion=e.entidad())
                h.save()
            else:
                # debia haber agresado pero no: anómalo
                h = models.Historial(estudiante=e,
                                     year=year,
                                     semestre=1,
                                     estado='anómalo',
                                     plan=plan,
                                     institucion=e.entidad())
                h.save()
