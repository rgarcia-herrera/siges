import tablib
import csv
from openpyxl import Workbook
from django.http import HttpResponse

from .models import Perfil, Academico, \
    GradoAcademico, Institucion, CampoConocimiento, \
    Adscripcion, LineaInvestigacion, tipos_membresia, estados_estudiante

from posgradmin.reporte_membresias_entidad import reporte


def reporte_membresias_entidad_ods(modeladmin, request, queryset):
    headers = (
        'membresía tipo',
        'estudiante cuenta',
        'estudiante nombre',
        'estudiante apellido',
        'estudiante correo',
        'estudiante generación',
        'estudiante estado',
        'tutor nombre',
        'tutor apellido',
        'tutor acreditación',
        'tutor correo',
        'tutor adscripción pcs',
    )
    data = []

    for e in queryset:
        data += reporte(e)

    tabla = tablib.Dataset(*data, headers=headers)

    response = HttpResponse(
        content=tabla.export('ods'),
        content_type='application/vnd.oasis.opendocument.spreadsheet',
        charset='utf-8')

    response['Content-Disposition'] = 'attachment; filename="membresias_entidad.ods"'

    return response


def reporte_disponibilidad_tutorias(modeladmin, request, queryset):
    response = HttpResponse(content_type='application/vnd.ms-excel', charset='utf-8')
    response['Content-Disposition'] = 'attachment; filename="disponibilidad_tutorias.xlsx"'

    wb = Workbook()
    ws = wb.active

    estados_ins = ['inscrito_maestría',
                 'suspensión_extraordinaria_maestría',
                 'suspensión_1_sem_maestría',
                 'suspensión_2_sem_maestría',
                 'inscrito_doctorado',
                 'suspensión_extraordinaria_doctorado',
                 'suspensión_1_sem_doctorado',
                 'suspensión_2_sem_doctorado',]

    estados_egr = ['egresado_maestría',
                 'egresado_doctorado',
                 'indeterminado',
                 'anómalo']

    tipos_tutor = ['TPD', 'TPM', 'CD', 'CM']
    tipos_miembro = ['MCD', 'MCM' ]

    header = ["académico",
              "acreditación",

              'inscritos tutoría principal',
              'egresados tutoría principal',

              'inscritos membresía de comité',
              'egresados membresía de comité',

              'indeterminados',
              'anómalos'
              ]
    ws.append(header)

    for a in queryset:
        estados = a.tutorias_conteo_estados()
        academico = str(a)
        row = [academico,
               a.acreditacion]

        inscritos_tutor = 0
        egresados_tutor = 0
        inscritos_miembro = 0
        egresados_miembro = 0
        indeterminados = 0
        anomalos = 0

        for tipo in tipos_tutor:
            for estado in estados_ins:
                if (tipo.endswith('M') and estado.endswith('maestría')
                    or
                    tipo.endswith('D') and estado.endswith('doctorado')):
                    inscritos_tutor += estados.get((tipo, estado), 0)

            for estado in estados_egr:
                if (tipo.endswith('M') and estado.endswith('maestría')
                    or
                    tipo.endswith('D') and estado.endswith('doctorado')):
                    egresados_tutor += estados.get((tipo, estado), 0)

        row.append(inscritos_tutor)
        row.append(egresados_tutor)

        for tipo in tipos_miembro:
            for estado in estados_ins:
                if (tipo.endswith('M') and estado.endswith('maestría')
                    or
                    tipo.endswith('D') and estado.endswith('doctorado')):
                    inscritos_miembro += estados.get((tipo, estado), 0)

            for estado in estados_egr:
                if (tipo.endswith('M') and estado.endswith('maestría')
                    or
                    tipo.endswith('D') and estado.endswith('doctorado')):
                    egresados_miembro += estados.get((tipo, estado), 0)

        row.append(inscritos_miembro)
        row.append(egresados_miembro)

        for tipo in tipos_tutor + tipos_miembro:
            indeterminados += estados.get((tipo, "indeterminado"), 0)
            anomalos += estados.get((tipo, "anómalo"), 0)

        row.append(indeterminados)
        row.append(anomalos)

        ws.append(row)

    wb.save(response)
    return response


def reporte_conteo_tutorias(modeladmin, request, queryset):

    response = HttpResponse(content_type='application/vnd.ms-excel', charset='utf-8')
    response['Content-Disposition'] = 'attachment; filename="conteo_tutorias.xlsx"'

    wb = Workbook()
    ws = wb.active

    row = ["académico",
           "acreditación", ]

    for tipo, tipo_label in tipos_membresia:
        row.append(f"{tipo} indeterminado")
        for estado, estado_label in estados_estudiante:
            if (tipo.endswith('M') and estado.endswith('maestría')
                or
                tipo.endswith('D') and estado.endswith('doctorado')):
                row.append(f"{tipo} {estado}")
    ws.append(row)

    for a in queryset:
        estados = a.tutorias_conteo_estados()
        academico = str(a)
        row = [academico,
               a.acreditacion]

        for tipo, tipo_label in tipos_membresia:
            row.append(estados.get((tipo, 'indeterminado'), 0))
            for estado, estado_label in estados_estudiante:
                if (tipo.endswith('M') and estado.endswith('maestría')
                    or
                    tipo.endswith('D') and estado.endswith('doctorado')):
                    row.append(estados.get((tipo, estado), 0))
        ws.append(row)

    wb.save(response)
    return response



def exporta_historico_tutorias(modeladmin, request, queryset):

    response = HttpResponse(content_type='application/vnd.ms-excel', charset='utf-8')
    response['Content-Disposition'] = 'attachment; filename="academicos_historico_tutorias.xlsx"'

    wb = Workbook()
    ws = wb.active

    row = ["académico",
           'estado del estudiante',
           'semestre',
           'tipo de membresía',
           'estudiante',
           'generacion']

    ws.append(row)

    for a in queryset:

        tm = [(m.estudiante.desenlace(plan="Maestría"),
               m.estudiante.gen_maestria(),
               m.tipo,
               m) for m in a.tutorias_estudiantes(plan="Maestría")]
        tm.sort(key=lambda m:m[3].year + m[3].semestre*0.1)
        td = [(m.estudiante.desenlace(plan="Doctorado"),
               m.estudiante.gen_doctorado(),
               m.tipo,
               m) for m in a.tutorias_estudiantes(plan="Doctorado")]
        td.sort(key=lambda m:m[3].year + m[3].semestre*0.1)

        for t in [tm, td]:
            for desenlace, gen, tipo, m in t:
                row = [str(a),
                       desenlace,
                       "%s-%s" % (m.year, m.semestre),
                       m.tipo,
                       m.estudiante.user.get_full_name(),
                       gen]

                ws.append(row)

    wb.save(response)
    return response



def exporta_resumen_academicos(modeladmin, request, queryset):

    response = HttpResponse(content_type='application/vnd.ms-excel', charset='utf-8')
    response['Content-Disposition'] = 'attachment; filename="academicos_resumen.xlsx"'

    wb = Workbook()
    ws = wb.active

    row = u"nombre|grado|acreditacion|adscripcion|asociacion|tesis_licenciatura|tesis_licenciatura_5|tesis_maestria|tesis_maestria_5|tesis_doctorado|tesis_doctorado_5|comite_doctorado_otros|comite_maestria_otros|participacion_comite_maestria|participacion_tutor_maestria|participacion_comite_doctorado|participacion_tutor_doctorado|articulos_internacionales_5|articulos_nacionales_5|articulos_internacionales|articulos_nacionales|capitulos|capitulos_5|libros|libros_5|palabras_clave|lineas|campos|faltantes".split("|")

    ws.append(row)


    for a in queryset:
        if a.perfil_personal_completo:

            # TODO: quitar lo de entidad_PCS y asociacion_PCS
            if a.user.perfil.adscripcion_set.filter(asociacion_PCS=True).count() > 0:
                asociacion = a.user.perfil.adscripcion_set.filter(asociacion_PCS=True).first()
                adscripcion = a.user.perfil.adscripcion_set.filter(asociacion_PCS=False).first()

            elif a.user.perfil.adscripcion_set.filter(institucion__entidad_PCS=True,
                                                    asociacion_PCS=False).count() > 0:
                adscripcion = a.user.perfil.adscripcion_set.filter(institucion__entidad_PCS=True,
                                                                   asociacion_PCS=False).first()
                asociacion = ""



        else:
            adscripcion = ""
            asociacion = ""

        grado = a.user.gradoacademico_set.last()
        if grado is not None:
            grado = grado.grado_obtenido
        else:
            grado = ""

        row = [
            a.user.get_full_name(),
            grado,
            a.acreditacion,
            str(adscripcion),
            str(asociacion).replace('None', ''),
            a.tesis_licenciatura,
            a.tesis_licenciatura_5,
            a.tesis_maestria,
            a.tesis_maestria_5,
            a.tesis_doctorado,
            a.tesis_doctorado_5,
            a.comite_doctorado_otros,
            a.comite_maestria_otros,
            a.participacion_comite_maestria,
            a.participacion_tutor_maestria,
            a.participacion_comite_doctorado,
            a.participacion_tutor_doctorado,
            a.articulos_internacionales_5,
            a.articulos_nacionales_5,
            a.articulos_internacionales,
            a.articulos_nacionales,
            a.capitulos,
            a.capitulos_5,
            a.libros,
            a.libros_5,
            a.palabras_clave.replace(u'\r\n', u';'),
            u";".join([l.nombre for l in a.lineas_de_investigacion.all()]),
            u";".join([c.nombre for c in a.campos_de_conocimiento.all()]),
            a.carencias_actividad().replace(u"\n", u";"),
        ]
        ws.append(row)

    wb.save(response)

    return response





def exporta_emails_cursos(modeladmin, request, queryset):

    response = HttpResponse(content_type='application/vnd.ms-excel', charset='utf-8')
    response['Content-Disposition'] = 'attachment; filename="correos_cursos.xlsx"'

    wb = Workbook()
    ws = wb.active

    ws.append(["Nombre del curso",
               "Año",
               "Semestre",
               "Nombre del profesor",
               "Correo del profesor"])

    for c in queryset:
        for a in c.academicos.all():
            row = [
                c.asignatura.asignatura,
                c.year,
                c.semestre,
                a.user.get_full_name(),
                a.user.email
            ]
            ws.append(row)

    wb.save(response)

    return response




def exporta_emails_estudiantes(modeladmin, request, queryset):

    response = HttpResponse(content_type='application/vnd.ms-excel', charset='utf-8')
    response['Content-Disposition'] = 'attachment; filename="correos_estudiantes.xlsx"'

    wb = Workbook()
    ws = wb.active

    ws.append(["Nombre",
               "Correo"])

    for e in queryset:
        row = [
            e.user.get_full_name(),
            e.user.email
        ]
        ws.append(row)

    wb.save(response)

    return response
