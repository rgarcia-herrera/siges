from django.contrib import admin

@admin.action(description="Actualizar últimas membresías")
def actualiza_ultimas_membresias(modeladmin, request,queryset):
    for m in queryset:
        m.actualiza_ultima()


@admin.action(description="Actualizar desde historial")
def actualiza_ultimo_estado_desde_historial(modeladmin, request, queryset):

    for e in queryset:
        e.microresumen = e.get_microresumen()
        e.entidad_pcs = e.ultima_entidad_pcs()
        e.actualiza_ultimo_estado_desde_historial()
