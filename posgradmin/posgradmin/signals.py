# coding: utf-8
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from posgradmin.models import Academico, Acreditacion, Curso, Historial, Graduado
from django.db.models.signals import m2m_changed


@receiver(pre_save, sender=Academico)
def academico_verifica_resumen_perfil(sender, **kwargs):
    """
    perfil academico actualiza unos booleanos
    """
    a = kwargs['instance']
    
    a.resumen_completo = a.verifica_resumen_completo()
    a.perfil_personal_completo = a.verifica_perfil_personal_completo()
    a.anexos_completo = a.verifica_anexos_completo()

    a.copia_ultima_acreditacion()
    
    a.semaforo_maestria = a.verifica_semaforo_maestria()
    a.semaforo_MCT_M = a.verifica_semaforo_MCT_M()    
    a.semaforo_doctorado = a.verifica_semaforo_doctorado()




@receiver(post_save, sender=Graduado)
def estado_graduado(sender, **kwargs):
    g = kwargs['instance']
    g.copia_estado_graduado()

    
@receiver(post_save, sender=Acreditacion)
def copia_acreditacion_a_academico(sender, **kwargs):
    ac = kwargs['instance']
    ac.academico.copia_ultima_acreditacion()
    ac.academico.save()


@receiver(post_save, sender=Historial)
def copia_plan_estado_entidad_a_estudiante(sender, **kwargs):
    hist = kwargs['instance']
    hist.estudiante.estado = hist.estudiante.ultimo_estado()
    hist.estudiante.plan = hist.estudiante.ultimo_plan()
    hist.estudiante.entidad_pcs = hist.estudiante.ultima_entidad_pcs()    
    hist.estudiante.save()
    

@receiver(post_save, sender=Curso)
def curso_genera_constancias(sender, **kwargs):
    c = kwargs['instance']
    c.genera_constancias()


def curso_academicos_changed(sender, **kwargs):
    if kwargs['action'] == "post_add":
        c = kwargs['instance']
        c.genera_constancias()

m2m_changed.connect(curso_academicos_changed, sender=Curso.academicos.through)



@receiver(post_save, sender=Acreditacion)
def genera_carta_acreditacion(sender, **kwargs):
    ac = kwargs['instance']
    ac.genera_carta()
