from django.core.management.base import BaseCommand
from posgradmin import models


class Command(BaseCommand):
    
    help = """

Suma un semestre a fechas escolares de egreso. Correr solo una vez!

https://codeberg.org/rgarcia-herrera/siges/issues/45

"""             

    def handle(self, *args, **options):

        suma()




def suma():
    for h in models.Historial.objects.filter(estado='egresado'):
        if h.semestre == 2:
            n_semestre = 1
            n_year = h.year + 1
        else:
            n_semestre = 2
            n_year = h.year
        print(f"{h.estudiante}, antes:{h.year}-{h.semestre} despues:{n_semestre}-{n_year}")
        h.year = n_year
        h.semestre = n_semestre
        h.save()

