from django.core.management.base import BaseCommand
from posgradmin import models


class Command(BaseCommand):
    
    help = """

Corrige año en inscritos a 2024-2
per issue #65

"""             

    def handle(self, *args, **options):
        reporte()




def inscripciones_maestria_recta_secuencia(e):
    gen_y, gen_s = [int(i) for i in e.generacion_maestria.split('-')]

    for y in range(gen_y, gen_y + 2):
        for s in (1,2):
            if e.historial.filter(estado='inscrito', plan='Maestría',
                                  year=y, semestre=s).count() != 1:
                return False

    if e.historial.filter(estado='inscrito', plan='Maestría').count() != 4:
        return False
            
    return True



def inscripciones_doctorado_recta_secuencia(e):
    gen_y, gen_s = [int(i) for i in e.generacion_doctorado.split('-')]

    for y in range(gen_y, gen_y + 4):
        for s in (1,2):
            if e.historial.filter(estado='inscrito', plan='Doctorado',
                                  year=y, semestre=s).count() != 1:
                return False

    if e.historial.filter(estado='inscrito', plan='Doctorado').count() != 8:
        return False
            
    return True


def reporte():

    print("cuenta,nombre,plan,estado,generacion_maestria,inscripciones_maestria,generacion_doctorado,inscripciones_doctorado")
    for e in models.Estudiante.objects.all().order_by('generacion_maestria', 'generacion_doctorado'):
        inscripciones_m = str(e.historial.filter(estado='inscrito', plan='Maestría').count())
        inscripciones_d = str(e.historial.filter(estado='inscrito', plan='Doctorado').count())

        
        if e.generacion_maestria:
            if not inscripciones_maestria_recta_secuencia(e):
                print(','.join([e.cuenta, e.user.get_full_name(), e.plan, e.estado,
                                e.generacion_maestria or '', inscripciones_m,
                                e.generacion_doctorado or '', inscripciones_d]))

        if e.generacion_doctorado:
            if not inscripciones_doctorado_recta_secuencia(e):
                print(','.join([e.cuenta, e.user.get_full_name(), e.plan, e.estado,
                                e.generacion_maestria or '', inscripciones_m,
                                e.generacion_doctorado or '', inscripciones_d]))

            
