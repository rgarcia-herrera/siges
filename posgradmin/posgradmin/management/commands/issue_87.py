from django.core.management.base import BaseCommand
from posgradmin import models
from datetime import datetime

class Command(BaseCommand):
    
    help = """
    https://codeberg.org/rgarcia-herrera/siges/issues/87
    eliminar del historial registros del 1 de diciembre 2021 inscrito 2023-2
    """             

    def handle(self, *args, **options):
        borra()


def borra():
    fecha = datetime(2021, 12, 1)
    print("borrando", models.Historial.objects.filter(fecha=fecha,
                                                      estado='inscrito').count())
    
    models.Historial.objects.filter(fecha=fecha,
                                    estado='inscrito').delete()

