from django.core.management.base import BaseCommand
from posgradmin import models
from datetime import datetime, date, timedelta
import argparse


class Command(BaseCommand):
    
    help = """
    per issue #59 en codeberg:
    Copiar estado "egresado" a tabla de estudiantes cuando sea ultimo registro en la tabla de historial

    Esto debido a que al registrar egresos no habiamos aplicado el
    parche de ordenar el historial por su ID consecutivo, y algunos
    quedaron como inscrito cuando son egresado.

    Correr solo una vez.

    """             

    def handle(self, *args, **options):

        recorre()




def recorre():
    for e in models.Estudiante.objects.all():
        if e.historial.all().last().estado == 'egresado' and e.estado != 'egresado':
            e.estado = 'egresado'
            e.save()
            print(e)

