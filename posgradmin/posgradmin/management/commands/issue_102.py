from django.core.management.base import BaseCommand
from posgradmin import models
import argparse
import csv
from datetime import date

class Command(BaseCommand):

    help = """
    cargar tabla de proyectos desde hoja de cálculo
    https://codeberg.org/rgarcia-herrera/siges/issues/102
    """


    def add_arguments(self, parser):
        parser.add_argument('--csv',
                            required=True,
                            type=argparse.FileType('r'),
                            help='path al archivo de proyectos formato csv')


    def handle(self, *args, **options):
        importa(options['csv'])




def busca_estudiante(cuenta):
    if models.Estudiante.objects.filter(cuenta=cuenta).count() > 0:
        return models.Estudiante.objects.filter(cuenta=cuenta).first()
    else:
        return None


def importa(proyectos):

    reader = csv.reader(proyectos, delimiter='|', quotechar='"')

    for i, row in enumerate(reader):
        if i == 0:
            continue
        if len(row) == 4:
            cuenta, titulo, plan, fecha = row

            estudiante = busca_estudiante(cuenta)
            if estudiante:
                dia, mes, anyo = fecha.split('-')
                fecha = date(int(anyo)+2000, int(mes), int(dia))

                if '4172' in plan:
                    plan = 'Maestría'
                elif '5172' in plan:
                    plan = 'Doctorado'

                if models.Proyecto.objects.filter(fecha=fecha, estudiante=estudiante, plan=plan).count() > 0:
                    p = models.Proyecto.objects.filter(fecha=fecha,
                                                       estudiante=estudiante,
                                                       plan=plan).order_by('id').last()
                    p.titulo = titulo
                    p.save()
                    print(f"[actualizado] {estudiante},{p},{fecha},{plan}")
                else:
                    p = models.Proyecto(fecha=fecha,
                                        titulo=titulo,
                                        estudiante=estudiante,
                                        plan=plan)
                    p.save()
                    print(f"[agregado] {estudiante},{p},{fecha},{plan}")
            else:
                print(f"[error] estudiante no encontrado {cuenta}")
