# coding: utf-8
from django.core.management.base import BaseCommand
from posgradmin import models
from os import path
from datetime import datetime, date
import argparse
from pyexcel_ods3 import get_data
from django.db.models import Q


class Command(BaseCommand):
    help = "Importa base de datos de graduados"

    def add_arguments(self, parser):
        parser.add_argument('ods_file',
                            type=argparse.FileType('r'),
                            help='path al archivo en formato ods')

    def handle(self, *args, **options):
        importa(options['ods_file'])



def busca_estudiante(cuenta):
    return models.Estudiante.objects.filter(cuenta=cuenta).first()


def busca_graduado(cuenta):
    estudiante = busca_estudiante(cuenta)
    return models.Graduado.objects.filter(estudiante=estudiante).first()


def busca_user(name):
   qs = models.User.objects.all()
   for term in name.split():
       qs = qs.filter( Q(first_name__icontains = term) | Q(last_name__icontains = term))
   if qs:
       return qs.first()
   else:
       return None


def busca_academico(nombre):
    u = busca_user(nombre)
    if u:
        return models.Academico.objects.filter(user=u).first()
    else:
        return None
        
    
def importa_graduacion(data):
    (cuenta, mencion, medalla,
     fecha_graduacion, year, semestre,
     plan, folio, modalidad, entidad) = data

    mencion = (mencion == "Sí")
    medalla = (medalla == "Sí")

    if '4172' in str(plan):
        plan = 'Maestría'
    elif '5172' in str(plan):
        plan = 'Doctorado'

    estudiante = busca_estudiante(cuenta)

    if "Tesis" in modalidad:
        modalidad = 'tesis'
    elif "Artículo" in modalidad:
        modalidad = 'artículo'
    elif "Reporte" in modalidad:
        modalidad = 'reporte técnico'
    elif "Protocolo" in modalidad:
        modalidad = 'protocolo de investigación doctoral'

    if estudiante:
        if models.Graduado.objects.filter(estudiante=estudiante, plan=plan).count() > 0:
            print("[warn] estudiante ya graduado", estudiante, plan)
            return None

        g = models.Graduado(
            estudiante=estudiante,
            mencion_honorifica=mencion,
            medalla_alfonso_caso=medalla,
            fecha=fecha_graduacion,
            year=year,
            semestre=semestre,
            plan=plan,
            folio_graduacion=folio,
            modalidad_graduacion=modalidad)
        g.save()
        g.copia_estado_graduado()
        estudiante.actualiza_ultimo_estado_desde_historial()
        print(g)
    else:
        print("[error] estudiante no encontrado", cuenta)



def importa_jurado(data, tipo="miembro"):
    (cuenta, nombre_academico, rol_asignado, rol_examen) = data

    rol_asignado = rol_asignado.strip().lower()
    rol_examen = rol_examen.strip().lower()
    
    graduado = busca_graduado(cuenta)
    if not graduado:
        print("[error] graduado no encontrado", cuenta)
        return False
        
    if tipo == "miembro":
        academico = busca_academico(nombre_academico)
        if not academico:
            print("[error] academico no encontrado", nombre_academico)
            return False
        
        m = models.MiembroJuradoGraduacion.objects.filter(
            graduado=graduado,
            academico=academico).delete()

        m = models.MiembroJuradoGraduacion.objects.filter(
            graduado=graduado,
            academico=academico)
        
        if m.count() == 0:
            m = models.MiembroJuradoGraduacion(
                graduado=graduado,
                academico=academico,
                rol_asignado=rol_asignado,
                rol_examen=rol_examen)
            m.save()
            
            print(m, rol_asignado, rol_examen)
        else:
            print("[warn] jurado ya registrado", graduado, academico, rol_asignado, rol_examen)
    elif tipo == "invitado":
        invitado, created = models.Invitado.objects.get_or_create(
            nombre = nombre_academico
        )

        if created:
            print("[warn] invitado nuevo", invitado)
        
        i = models.InvitadoJuradoGraduacion.objects.filter(
            graduado=graduado,
            invitado=invitado).delete()

        i = models.InvitadoJuradoGraduacion.objects.filter(
            graduado=graduado,
            invitado=invitado)
        
        if i.count() == 0:
            i = models.InvitadoJuradoGraduacion(
                graduado=graduado,
                invitado=invitado,
                rol_asignado=rol_asignado,
                rol_examen=rol_examen)
            i.save()
            
            print(i, rol_asignado, rol_examen)
        else:
            print("[warn] invitado a jurado ya registrado", graduado, invitado, rol_asignado, rol_examen)
        


def importa(db_file):
    data = get_data(db_file.name)

    for i in range(1, len(data['alumnos'])):
        if len(data['alumnos'][i]) == 10:
            importa_graduacion(data['alumnos'][i])

    print('======== academicos en jurado')

    for i in range(1, len(data['jurados'])):
        if len(data['jurados'][i]) == 4:
            importa_jurado(data['jurados'][i], tipo='miembro')

    print('======== invitados en jurado')
            
    for i in range(1, len(data['invitados'])):
        if len(data['invitados'][i]) == 4:
            importa_jurado(data['invitados'][i], tipo='invitado')
            
