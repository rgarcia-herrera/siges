from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from posgradmin import models
from os import path
from datetime import datetime, date
import argparse
import csv
import logging


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Importa registros de "inscrito" 2023-2" a historial desde hoja de cálculo'

    def add_arguments(self, parser):
        parser.add_argument('--csv',
                            required=True,
                            type=argparse.FileType('r'),
                            help='path al archivo formato csv')




    def handle(self, *args, **options):

        importa(options['csv'])



def get_institucion(entidad_num):

    entidad_num = int(entidad_num)
    
    if entidad_num == 600:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Escuela Nacional de Estudios Superiores Unidad León",
                                              entidad_PCS=True)
    if entidad_num == 700:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Escuela Nacional de Estudios Superiores Unidad Morelia",
                                              entidad_PCS=True)
    if entidad_num == 1:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Facultad de Arquitectura ",
                                              entidad_PCS=True)
    if entidad_num == 3:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Facultad de Ciencias",
                                              entidad_PCS=True)
    if entidad_num == 65:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Biología",
                                              entidad_PCS=True)
    if entidad_num == 67:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Ciencias del Mar y Limnología",
                                              entidad_PCS=True)
    if entidad_num == 69:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Ecología",
                                              entidad_PCS=True)
    if entidad_num == 90:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Energías Renovables",
                                              entidad_PCS=True)
    if entidad_num == 11:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Ingeniería",
                                              entidad_PCS=True)
    if entidad_num == 79:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Investigaciones Económicas",
                                              entidad_PCS=True)
    if entidad_num == 87:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Investigaciones Sociales",
                                              entidad_PCS=True)
    if entidad_num == 97:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Investigaciones en Ecosistemas y Sustentabilidad",
                                              entidad_PCS=True)

        


def importa(f):
    reader = csv.DictReader(f)

    for row in reader:
        
        cuenta = row['cuenta']

        try:
            e = models.Estudiante.objects.get(cuenta=cuenta)
        except ObjectDoesNotExist:
            logger.error(f'no hay estudiante con cuenta {cuenta}')
            continue


        if models.Historial.objects.filter(estudiante=e,
                                           year=2023,
                                           semestre=2,
                                           estado='inscrito').count() == 0:
            if row['plan'] == '4172':
                plan = 'Maestría'
            elif row['plan'] == '5172':
                plan = 'Doctorado'

            
            h = models.Historial(
                estudiante=e,
                year=2023,
                semestre=2,
                estado='inscrito',
                plan=plan,
                institucion=get_institucion(entidad_num=row['entidad']))

            h.save()
            logger.warning(f"registrado {h}")
        else:
            logger.warning(f"ya hay registro para {row}")

                
