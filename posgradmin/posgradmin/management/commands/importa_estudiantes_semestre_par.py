from django.core.management.base import BaseCommand
from posgradmin import models
from os import path
from datetime import datetime, date
import argparse
import csv
import jellyfish
from pprint import pprint

class Command(BaseCommand):
    help = 'Importa estudiantes en semestres pares, i.e. en febrero'

    def add_arguments(self, parser):
        parser.add_argument('--reinscritos',
                            required=True,
                            type=argparse.FileType('r'),
                            help='path al archivo en formato csv')

    def handle(self, *args, **options):

        importa(options['reinscritos'])




def importa(reinscritos):
    reader = csv.DictReader(reinscritos)
    
    for idx, row in enumerate(reader):
        # cuenta,NOMBRE,email,plan,campo,ENTIDAD,tipo,telefonos


        if models.Estudiante.objects.filter(cuenta=row['cuenta']).count() == 0:
            print("[ERROR] fila", idx, row['cuenta'], 'cuenta no encontrada, imposible reingresar estudiante sin registro previo')
            continue

        e = models.Estudiante.objects.get(cuenta=row['cuenta'])
        year = date.today().year
        semestre = 2

        plan = row['plan']
        if plan == '4172':
            plan = 'Maestría'
        elif plan == '5172':
            plan = 'Doctorado'
        else:
            print(f'[ERROR] fila {idx} plan no válido {plan}')

        h, created = models.Historial.objects.get_or_create(
            fecha = date.today(),
            estudiante = e,
            year = year,
            semestre = semestre,
            plan = plan,
            estado = 'inscrito',
        )


        entidad = int(row['entidad'].split()[0])

        h.institucion = get_institucion(entidad)
        h.save()

        e.estado = e.ultimo_estado()
        e.plan = e.ultimo_plan()
        e.save()

        print('historial actualizado', h)





def get_institucion(entidad_num):
    if entidad_num == 600:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Escuela Nacional de Estudios Superiores Unidad León",
                                              entidad_PCS=True)
    if entidad_num == 700:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Escuela Nacional de Estudios Superiores Unidad Morelia",
                                              entidad_PCS=True)
    if entidad_num == 1:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Facultad de Arquitectura ",
                                              entidad_PCS=True)
    if entidad_num == 3:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Facultad de Ciencias",
                                              entidad_PCS=True)
    if entidad_num == 65:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Biología",
                                              entidad_PCS=True)
    if entidad_num == 67:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Ciencias del Mar y Limnología",
                                              entidad_PCS=True)
    if entidad_num == 69:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Ecología",
                                              entidad_PCS=True)
    if entidad_num == 90:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Energías Renovables",
                                              entidad_PCS=True)
    if entidad_num == 11:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Ingeniería",
                                              entidad_PCS=True)
    if entidad_num == 79:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Investigaciones Económicas",
                                              entidad_PCS=True)
    if entidad_num == 87:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Investigaciones Sociales",
                                              entidad_PCS=True)
    if entidad_num == 97:
        return models.Institucion.objects.get(nombre="Universidad Nacional Autónoma de México",
                                              suborganizacion="Instituto de Investigaciones en Ecosistemas y Sustentabilidad",
                                              entidad_PCS=True)

