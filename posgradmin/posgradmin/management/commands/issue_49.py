from django.core.management.base import BaseCommand
from posgradmin import models


class Command(BaseCommand):
    
    help = """ En retrospectiva: los registros de "graduado" en la tabla de
    graduados que ocurran en cualquier mes anterior al último mes del
    semestre escolar de la última inscripción del estudiante deben
    reescribirse como "graduado anticipado".

    https://codeberg.org/rgarcia-herrera/siges/issues/49
    """

    def handle(self, *args, **options):

        anticipados()




def anticipados():
    for g in models.Graduado.objects.all():
        l = g.estudiante.historial.filter(estado="inscrito", plan=g.plan).last()
        if l.year >= g.year:
            if l.semestre >= g.semestre:
                if g.estudiante.historial.filter(estado="graduado", plan=g.plan).count() > 0:
                    h_graduado = g.estudiante.historial.filter(estado="graduado", plan=g.plan).last()
                    h_graduado.estado = "graduado anticipado"
                    h_graduado.save()

                    print(f"{g.estudiante} tuvo graduacion anticipada")


