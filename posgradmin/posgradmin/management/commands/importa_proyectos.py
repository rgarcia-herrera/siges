# coding: utf-8
from django.core.management.base import BaseCommand
from posgradmin import models
from datetime import datetime, date
import csv
import argparse


class Command(BaseCommand):
    help = u'Importa base de datos de proyectos'

    def add_arguments(self, parser):
        parser.add_argument('--proyectos',  # dgae
                            required=True,
                            type=argparse.FileType('r'),
                            help='path al archivo de proyectos en formato ods')


    def handle(self, *args, **options):

        importa(options['proyectos'])


def busca_estudiante(cuenta):
    return models.Estudiante.objects.filter(cuenta=cuenta).first()

        
def importa(proyectos):

    reader = csv.reader(proyectos, delimiter='|', quotechar='"')

    for row in reader:
        if len(row) == 4:
            cuenta, titulo, plan, fecha = row
            anyo, mes, dia = fecha.split('-')
            fecha = date(int(anyo), int(mes), int(dia))

            if '4172' in plan:
                plan = 'Maestría'
            elif '5172' in plan:
                plan = 'Doctorado'
            
            p = models.Proyecto(fecha=fecha,
                                titulo=titulo,
                                estudiante=busca_estudiante(cuenta),
                                plan=plan)
            p.save()
            print(p)
    
