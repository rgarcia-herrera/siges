from django.core.management.base import BaseCommand
from posgradmin import models


class Command(BaseCommand):
    
    help = """

    Quita registros de "egresado" del historial
    
    per issue #86
    
    https://codeberg.org/rgarcia-herrera/siges/issues/86
    """             

    def handle(self, *args, **options):

        desegresa()




def desegresa():
    for h in models.Historial.objects.filter(estado="egresado"):
        h.delete()

