import os
import pandas as pd
from pandas.plotting import parallel_coordinates

from collections import defaultdict

import datetime

from django.template.defaultfilters import slugify
from django.db import models
from django.db.models import Q

from django.contrib.auth.models import User

from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from .settings import MEDIA_URL, \
    APP_PREFIX, MEDIA_ROOT, BASE_DIR

from wordcloud import WordCloud

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from tempfile import NamedTemporaryFile
from django.template.loader import render_to_string
from sh import rm
import subprocess
import pathlib
from pdfrw import PdfReader, PdfWriter, PageMerge


def nota_path(instance, filename):
    (root, ext) = os.path.splitext(filename)
    return os.path.join(u'nota_%s_%s' % (instance.id, slugify(root) + ext))


class Nota(models.Model):
    asunto = models.CharField(max_length=100)
    nota = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True)
    autor = models.ForeignKey(User,
                              on_delete=models.CASCADE, null=True, blank=True)

    archivo = models.FileField(upload_to=nota_path,
                               null=True, blank=True)
    estado = models.CharField(max_length=40,
                              default='memo',
                              choices=((u"memo", u"memo"),
                                       (u"solicitud",
                                        u"solicitud"),
                                       ("pendiente", "pendiente"),
                                       ("atendida", "atendida"),
                                       ("cancelada", "cancelada")
                                       ))

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return "%s, fecha: %s" % (self.autor,
                                      self.fecha.strftime("%Y-%m-%d %H:%M"))




class ConvocatoriaCurso(models.Model):
    year = models.PositiveSmallIntegerField("Año")
    semestre = models.PositiveSmallIntegerField(
        choices=((1, 1), (2, 2)))
    status = models.CharField(max_length=10,
            choices=[('abierta', 'abierta'),
                     ('rev CA', 'revisión CA'),
                     ('cerrada', 'cerrada')])

    notas = GenericRelation(Nota,
                           related_query_name='convocatoria_curso')


    def __str__(self):
        if self.status == 'abierta':
            status = '(abierta)'
        else:
            status = ''
        return u"%s-%s %s" % (self.year,
                              self.semestre,
                              status)

    class Meta:
        verbose_name_plural = "Convocatorias para cursos"
        unique_together = ('year', 'semestre')
        ordering = ['year', 'semestre', ]


class Institucion(models.Model):
    nombre = models.CharField("Institución u Organización", max_length=150)
    suborganizacion = models.CharField(
        "Dependencia, Entidad o Departamento", max_length=150)
    pais = models.CharField("País", max_length=100, blank=True)
    estado = models.CharField(max_length=100, blank=True)
    dependencia_UNAM = models.BooleanField(default=False)


    def __str__(self):
        return u"%s, %s" % (self.nombre, self.suborganizacion)

    class Meta:
        verbose_name_plural = "instituciones"
        unique_together = ('nombre', 'suborganizacion', 'estado', 'pais')
        ordering = ['nombre', 'suborganizacion', ]




class EntidadPCS(models.Model):
    nombre = models.CharField("Institución u Organización", max_length=150)
    suborganizacion = models.CharField(
        "Dependencia, Entidad o Departamento", max_length=150)

    def __str__(self):
        return self.suborganizacion

    class Meta:
        verbose_name_plural = "Entidades PCS"
        ordering = ['nombre', 'suborganizacion', ]




class CampoConocimiento(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return u"%s" % self.nombre

    class Meta:
        verbose_name_plural = "campos de conocimiento"


class LineaInvestigacion(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return u"%s" % self.nombre

    class Meta:
        verbose_name_plural = "líneas de investigación"


def headshot_path(instance, filename):
    extension = filename.split('.')[-1]
    return u'headshots/%s.%s' % (instance.user.id, extension)


class Perfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    curp = models.CharField("CURP. Si usted es extranjero(a) y no cuenta con "
                            + "este dato, ingresar la palabra extranjero(a)",
                            max_length=100)
    rfc = models.CharField("RFC. Si usted es extranjero(a) y no cuenta con "
                           + "este dato, ingresar la palabra extranjero(a)",
                           max_length=100)

    CVU = models.CharField(u"Número de CVU",
                           max_length=100, blank=True, null=True)

    telefono = models.CharField(max_length=100)
    telefono_movil = models.CharField(max_length=100, blank=True)

    direccion1 = models.CharField("dirección del lugar de trabajo",
                                  max_length=350)

    codigo_postal = models.PositiveSmallIntegerField(default=0)

    website = models.CharField(max_length=200, blank=True)

    genero = models.CharField(max_length=1, choices=(('M', 'masculino'),
                                                     ('F', 'femenino')))

    nacionalidad = models.CharField(max_length=100)

    fecha_nacimiento = models.DateField('fecha de nacimiento',
                                        blank=True, null=True)

    headshot = models.ImageField("fotografía",
                                 upload_to=headshot_path,
                                 blank=True, null=True)

    notas = GenericRelation(Nota,
                           related_query_name='perfil')


    def __str__(self):
        return u"%s" % self.user.get_full_name()

    def __repr__(self):
        return self.__str__()

    class Meta:
        verbose_name_plural = "Perfiles Personales"
        ordering = ['user__first_name', 'user__last_name', ]


    def adscripcion(self):
        return self.adscripcion_set.order_by('anno_nombramiento').last()


    def adscripcion_ok(self):
        """
        tiene adscripcion o adscripcion pcs
        """

        if self.adscripcion_set.count() > 0:
            return True

        if hasattr(self.user, 'academico'):
            if self.user.academico.adscripcion_pcs:
                return True

        return False


    def perfil_publico_anchor(self):
        return u"""<a href='%sinicio/perfil/publico/%s'>%s</a>""" % (
            APP_PREFIX,
            self.user.get_username(), self.__str__())

    def perfil_comite_anchor(self):
        return u"""<a href='%sinicio/perfil/comite/%s'>🗫</a>""" % (
            APP_PREFIX,
            self.user.get_username())



class Invitado(models.Model):
    nombre = models.CharField(max_length=333)
    correo = models.EmailField(blank=True, null=True)
    descripcion = models.TextField(blank=True)

    class Meta:
        verbose_name_plural = "Invitados externos"
        verbose_name = "Invitado(a) Externo(a)"

    def __str__(self):
        return self.nombre


class GradoAcademico(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    nivel = models.CharField(max_length=15,
                             choices=(('licenciatura', 'licenciatura'),
                                      ('maestria', 'maestria'),
                                      ('doctorado', 'doctorado')))

    grado_obtenido = models.CharField(max_length=100)

    institucion = models.ForeignKey(Institucion, on_delete=models.CASCADE)

    fecha_obtencion = models.DateField("Fecha de obtención de grado")

    def __str__(self):
        return u"%s @ %s" % (self.grado_obtenido, self.institucion)

    class Meta:
        verbose_name_plural = "Grados académicos"
        ordering = ("-fecha_obtencion",)



estados_estudiante = (
    ('inscrito_maestría', 'inscrito maestría'),
    ('egresado_maestría', 'egresado maestría'),
    ('graduado_maestría', 'graduado maestría'),
    ('graduado_anticipado_maestría', 'graduado anticipado maestría'),
    ('baja_maestría', 'baja maestría'),
    ('suspensión_extraordinaria_maestría', 'suspensión extraordinaria maestría'),
    ('suspensión_1_sem_maestría', 'suspensión 1 sem maestría'),
    ('suspensión_2_sem_maestría', 'suspensión 2 sem maestría'),

    ('inscrito_doctorado', 'inscrito doctorado'),
    ('egresado_doctorado', 'egresado doctorado'),
    ('graduado_doctorado', 'graduado doctorado'),
    ('graduado_anticipado_doctorado', 'graduado anticipado doctorado'),
    ('baja_doctorado', 'baja doctorado'),
    ('suspensión_extraordinaria_doctorado', 'suspensión extraordinaria doctorado'),
    ('suspensión_1_sem_doctorado', 'suspensión 1 sem doctorado'),
    ('suspensión_2_sem_doctorado', 'suspensión 2 sem doctorado'),

    ('movilidad', 'movilidad'),
    ('indeterminado', 'indeterminado'),
    ('anómalo', 'anómalo'),
)


class Estudiante(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    cuenta = models.CharField(max_length=100)

    estado = models.CharField(
        max_length=40,
        choices=estados_estudiante,
        null=True, blank=True)

    plan = models.CharField(
        max_length=20,
        choices=[("Maestría", "Maestría"),
                 ("Doctorado", "Doctorado")],
        default="Maestría"
    )

    generacion_doctorado = models.CharField(max_length=7,
                                            blank=True, null=True)
    generacion_maestria = models.CharField(max_length=7,
                                            blank=True, null=True)

    doctorado_directo = models.BooleanField(default=False)

    opcion_titulacion = models.BooleanField("Opción a titulación",
                                            default=False)

    opcion_graduacion = models.BooleanField("Opción a graduación",
                                            default=False)

    campo_conocimiento = models.ForeignKey(CampoConocimiento,
                                           blank=True, null=True,
                                           on_delete=models.CASCADE)
    lineas_investigacion = models.ForeignKey(LineaInvestigacion,
                                             blank=True, null=True,
                                             on_delete=models.CASCADE)

    notas = GenericRelation(Nota,
                           related_query_name='estudiante')

    microresumen = models.CharField(max_length=50,
                                    blank=True, null=True)

    entidad_pcs = models.ForeignKey(EntidadPCS, on_delete=models.CASCADE,
                                    help_text=u"Entidad PCS",
                                    blank=True, null=True)


    def ultima_entidad_pcs(self):
        if self.historial.count() > 0:
            h = self.historial.order_by('year', 'semestre', 'id').last()
            return h.entidad_pcs
        else:
            return None


    class Meta:
        ordering = ['user__first_name', 'user__last_name', ]


    def get_microresumen(self):

        sigla_m = { 'inscrito_maestría': 'i',
                    'egresado_maestría': 'e',
                    'graduado_maestría': 'g',
                    'graduado_anticipado_maestría': 'G',
                    'baja_maestría': 'b',
                    'suspensión_extraordinaria_maestría': 'x',
                    'suspensión_1_sem_maestría': '1',
                    'suspensión_2_sem_maestría': '2',

                    'movilidad': 'm',
                    'indeterminado': 'n',
                    'anómalo': 'a'}

        M = [sigla_m[s.estado]
             for s in self.historial.order_by('year', 'semestre', 'id').filter(plan='Maestría')]

        sigla_d = { 'inscrito_doctorado': 'i',
                    'egresado_doctorado': 'e',
                    'graduado_doctorado': 'g',
                    'graduado_anticipado_doctorado': 'G',
                    'baja_doctorado': 'b',
                    'suspensión_extraordinaria_doctorado': 'x',
                    'suspensión_1_sem_doctorado': '1',
                    'suspensión_2_sem_doctorado': '2',

                    'movilidad': 'm',
                    'indeterminado': 'n',
                    'anómalo': 'a'}


        D = [sigla_d[s.estado]
             for s in self.historial.order_by('year', 'semestre', 'id').filter(plan='Doctorado')]

        resumen = ""
        if M:
            resumen += 'M' + "".join(M)

        if D:
            resumen += 'D' + "".join(D)

        return resumen


    def ultimo_plan(self):
        if self.historial.count() > 0:
            h = self.historial.order_by('year', 'semestre', 'id').last()
            plan = h.plan
        else:
            plan = None

        return plan


    def ultimo_proyecto(self, plan=None):
        if plan is None:
            plan = self.ultimo_plan()

        if self.proyecto_set.filter(plan=plan).count() > 0:
            p = self.proyecto_set.filter(plan=plan).latest('fecha')
        else:
            p = None

        return p


    def ultimo_estado(self):
        if self.historial.count() > 0:
            h = self.historial.order_by('year', 'semestre', 'id').last()
            ultimo_estado = h.estado
        else:
            ultimo_estado = None

        return ultimo_estado

    def desenlace(self, plan):
        return self.ultimo_estado_plan(plan)

    def ultimo_estado_plan(self, plan):
        if self.historial.filter(plan=plan).count() > 0:
            h = self.historial.filter(plan=plan).order_by('year', 'semestre', 'id').last()
            ultimo_estado = h.estado
        else:
            ultimo_estado = None

        return ultimo_estado


    def actualiza_ultimo_estado_desde_historial(self):
        self.estado = self.ultimo_estado()
        self.save()

    def comite_vigente(self):
        return self.get_comite(self.ultimo_plan())


    def get_historial(self, plan):
        return self.historial.filter(plan=plan).order_by('-year', '-semestre', '-fecha')


    def historial_maestria(self):
        return self.get_historial('Maestría')


    def historial_doctorado(self):
        return self.get_historial('Doctorado')


    def apoyos(self, plan):
        return self.apoyomovilidad_set.filter(plan=plan)

    def apoyos_maestria(self):
        return self.apoyos(plan="Maestría")

    def apoyos_doctorado(self):
        return self.apoyos(plan="Doctorado")

    def estado_doctorado(self):
        h = self.historial_doctorado()
        return h[0].estado

    def estado_maestria(self):
        h = self.historial_maestria()
        return h[0].estado

    def get_comite(self, plan, con_invitados=True):
        tutores = []
        invitados = []
        if plan == 'Doctorado':
            tipos = ['TPD', 'CD', 'MCD']
            tipo_invitado = 'AED'
        elif plan == "Maestría":
            tipos = ['TPM', 'CM', 'MCM']
            tipo_invitado = 'AEM'

        if self.miembros_comite.filter(tipo__in=tipos).count() > 0:
            m = self.miembros_comite.filter(
                tipo__in=tipos).order_by('year', 'semestre').last()
            y = m.year
            s = m.semestre

            tutores = list(self.miembros_comite.filter(tipo__in=tipos,
                                                       year=y, semestre=s).order_by('year', 'semestre'))
        else:
            return []

        if con_invitados:
            invitados = list(self.invitadomembresiacomite_set.filter(tipo=tipo_invitado,
                                                                     year=y,
                                                                     semestre=s))

            return tutores + invitados
        else:
            return tutores

    def comite_maestria(self):
        return self.get_comite('Maestría')

    def comite_doctorado(self):
        return self.get_comite('Doctorado')

    def ultima_inscripcion(self, plan):
        h = self.historial.filter(plan=plan,
                                  estado__startswith='inscrito').order_by('year', 'semestre')

        if h.count() > 0:
            return h.last()
        else:
            return None


    def ultima_inscripcion_maestria(self):
        h = self.ultima_inscripcion('Maestría')
        y = h.year
        s = h.semestre

        return f"{y}-{s}"


    def ultima_inscripcion_doctorado(self):
        h = self.ultima_inscripcion('Doctorado')
        y = h.year
        s = h.semestre

        return f"{y}-{s}"



    def generacion(self, plan):
        if self.historial.filter(plan=plan).count() > 0:
            h = self.historial.filter(
                plan=plan).order_by('year', 'semestre').first()
            y = h.year
            s = h.semestre
            return f"{y}-{s}"
        else:
            return None

    def gen_maestria(self):
        gen = self.generacion_maestria
        if not gen:
            self.generacion_maestria = self.generacion(plan="Maestría")
            self.save()
            gen = self.generacion_maestria
        return gen

    def gen_doctorado(self):
        gen = self.generacion_doctorado
        if not gen:
            self.generacion_doctorado = self.generacion(plan="Doctorado")
            self.save()
            gen = self.generacion_doctorado
        return gen

    def sede_administrativa(self, plan):
        return self.sedes.filter(plan=plan).first()

    def sede_adm_maestria(self):
        return self.sede_administrativa('Maestría')

    def sede_adm_doctorado(self):
        return self.sede_administrativa('Doctorado')


    def proyecto(self, plan):
        return self.proyecto_set.filter(plan=plan).order_by('fecha').last()

    def proyecto_maestria(self):
        return self.proyecto('Maestría')

    def proyecto_doctorado(self):
        return self.proyecto('Doctorado')



    def faltan_documentos(self):
        if self.user.gradoacademico_set.count() == 0:
            return True
        else:
            return False

    def __str__(self):
        return u"%s (%s)" % (self.user.get_full_name(),
                                self.cuenta)


    def perfil_plan(self, plan):
        if self.historial.filter(plan=plan).count == 0:
            return None

        return {
            'proyecto': self.ultimo_proyecto(plan),
            'sede': self.sedes.filter(plan=plan),
            'tema': (self.lineas_investigacion if plan == 'Doctorado'
                     else self.campo_conocimiento),
            'historial': self.historial.filter(
                plan=plan).order_by('year', 'semestre')
        }


    def ficha_a(self):
        return u"""<a href='%sestudiante/%s'>%s</a>""" % (APP_PREFIX,
                                                          self.cuenta,
                                                          self.cuenta)

    def as_a(self):
        return "<a href='%sinicio/usuario/%s/'>%s</a>" % (
            APP_PREFIX,
            self.user.id,
            self.user.get_full_name())


class Graduado(models.Model):
    estudiante = models.ForeignKey(Estudiante,
                                   related_name='graduaciones',
                                   on_delete=models.CASCADE)

    mencion_honorifica = models.BooleanField(default=False)
    medalla_alfonso_caso = models.BooleanField(u"Postulación a Medallista Alfonso Caso",
                                               default=False)

    fecha = models.DateField("fecha de graduación",
                             default=datetime.date.today)

    year = models.PositiveSmallIntegerField("año",
                                            blank=True, null=True)
    semestre = models.PositiveSmallIntegerField(
        "semestre",
        choices=((1, 1),
                 (2, 2)))


    plan = models.CharField(
        max_length=20,
        choices=((u"Maestría", u"Maestría"),
                 (u"Doctorado", u"Doctorado")))

    folio_graduacion = models.CharField(u"Folio de acta de examen de grado",
                                        max_length=200, blank=True)

    modalidad_graduacion = models.CharField(
        max_length=35,
        default='-',
        choices=(
            ('-', '-'),
            ('tesis', 'tesis'),
            ('reporte técnico', 'reporte técnico'),
            ('artículo', 'artículo'),
            ('protocolo de investigación doctoral', 'protocolo de investigación doctoral'),
        ))

    notas = GenericRelation(Nota,
                            related_query_name='historial')

    @property
    def proyecto(self):
        return Proyecto.objects.filter(plan=self.plan,
                                       estudiante=self.estudiante).order_by('id').last()


    def copia_estado_graduado(self):
        """ al guardar en este modelo se agrega un registro a la tabla de historial """

        # Si ya hay registro de graduado en historial, no hacer nada
        if self.estudiante.historial.filter(estado__startswith='graduado',
                                            plan=self.plan).count() > 0:
            return
        if self.estudiante.historial.filter(estado__startswith='graduado_anticipado',
                                            plan=self.plan).count() > 0:
            return

        # graduado anticipado ocurre si la fecha de graduacion es anterior al ultimo mes del semestre escolar de la ultima inscripcion
        anticipado = False
        last_ins = self.estudiante.historial.filter(plan=self.plan, estado__startswith='inscrito').last()
        if (last_ins.year >= self.year):
            if (last_ins.semestre >= self.semestre):
                anticipado = True

        if anticipado:
            estado = "graduado_anticipado_" + self.plan.lower()
        else:
            estado = "graduado_" + self.plan.lower()


        h = Historial()
        h.estudiante = self.estudiante
        h.year = self.year
        h.semestre = self.semestre
        h.plan = self.plan
        h.estado = estado
        h.institucion = self.estudiante.entidad_pcs
        h.save()

    def __str__(self):
        return f"{self.estudiante} {self.plan}"


class InvitadoJuradoGraduacion(models.Model):
    graduado = models.ForeignKey(Graduado, on_delete=models.CASCADE)
    invitado = models.ForeignKey(Invitado,
                                 on_delete=models.CASCADE)

    rol_asignado = models.CharField(
        max_length=25,
        default='D',
        choices=(
            ('presidente', 'presidente'),
            ('secretario', 'secretario'),
            ('vocal 1', 'vocal 1'),
            ('vocal', 'vocal'),
        )
    )

    rol_examen = models.CharField(
        max_length=25,
        default='D',
        choices=(
            ('presidente', 'presidente'),
            ('secretario', 'secretario'),
            ('vocal 1', 'vocal 1'),
            ('vocal', 'vocal'),
            ('ausente', 'ausente'),
        )
    )


    class Meta:
        verbose_name_plural = "Invitaciones a Jurados de Graduación"

    def __str__(self):
        return f"{self.graduado} {self.invitado}"



class MiembroJuradoGraduacion(models.Model):
    graduado = models.ForeignKey(Graduado,
                                 on_delete=models.CASCADE)
    academico = models.ForeignKey("Academico",
                                  on_delete=models.CASCADE)

    rol_asignado = models.CharField(
        max_length=25,
        default='D',
        choices=(
            ('presidente', 'presidente'),
            ('secretario', 'secretario'),
            ('vocal 1', 'vocal 1'),
            ('vocal', 'vocal'),
        )
    )

    rol_examen = models.CharField(
        max_length=25,
        default='D',
        choices=(
            ('presidente', 'presidente'),
            ('secretario', 'secretario'),
            ('vocal 1', 'vocal 1'),
            ('vocal', 'vocal'),
            ('ausente', 'ausente'),
        )
    )

    class Meta:
        verbose_name_plural = "Académicos en Jurados de Graduación"

    def __str__(self):
        return f"{self.graduado} {self.academico}"



class Sede(models.Model):
    estudiante = models.ForeignKey(Estudiante, related_name='sedes', on_delete=models.CASCADE)
    plan = models.CharField(
        max_length=20,
        choices=((u"Maestría", u"Maestría"),
                 (u"Doctorado", u"Doctorado")
                 ))
    sede = models.CharField(
        max_length=20,
        choices=(("CDMX", "CDMX"),
                 ("León", "León"),
                 ("Morelia (IIES)", "Morelia (IIES)"),
                 ("Morelia (ENES)", "Morelia (ENES)"),
                 ))

    class Meta:
        verbose_name_plural = "Sedes administrativas"
        unique_together = ('estudiante', 'plan')


    def __str__(self):
        return f"{self.sede}"



class Historial(models.Model):
    estudiante = models.ForeignKey(Estudiante, related_name='historial', on_delete=models.CASCADE)

    fecha = models.DateField("fecha del registro",
                             help_text=u"fecha del registro en la bitácora",
                             default=datetime.date.today)

    year = models.PositiveSmallIntegerField("año",
                                            blank=True, null=True)
    semestre = models.PositiveSmallIntegerField(
        "semestre",
        choices=((1, 1),
                 (2, 2)))

    estado = models.CharField(
        max_length=40,
        null=True, blank=True,
        choices=estados_estudiante)

    entidad_pcs = models.ForeignKey(EntidadPCS, on_delete=models.CASCADE,
                                    help_text=u"Entidad PCS",
                                    blank=True, null=True)


    plan = models.CharField(
        max_length=20,
        choices=((u"Maestría", u"Maestría"),
                 (u"Doctorado", u"Doctorado")))

    notas = GenericRelation(Nota,
                            related_query_name='historial')


    class Meta:
        verbose_name_plural = "Historial"
        ordering = ['year', 'semestre', 'id',]

    def __str__(self):
        return f"{self.fecha}: {self.estudiante} [{self.estado}] {self.year}-{self.semestre}"


class ApoyoMovilidad(models.Model):
    estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=200, help_text="Nombre de la actividad")

    fecha_solicitud = models.DateField("Fecha de solicitud", default=datetime.date.today)

    plan = models.CharField(
        max_length=20,
        choices=[("Maestría", "Maestría"),
                 ("Doctorado", "Doctorado")],
        default="Maestría"
    )

    year = models.PositiveSmallIntegerField("año",
                                            blank=True, null=True)

    semestre = models.PositiveSmallIntegerField(
        "semestre",
        choices=((1, 1),
                 (2, 2)))

    fecha_inicio = models.DateField(blank=True, null=True)
    fecha_fin = models.DateField(blank=True, null=True)

    institucion = models.ForeignKey(Institucion, on_delete=models.CASCADE)
    internacional = models.BooleanField(default=True)
    ciudad = models.CharField(max_length=200, blank=True, null=True)
    pais = models.CharField(max_length=200, blank=True, null=True)

    monto_otorgado = models.DecimalField("Monto otorgado en pesos mexicanos", max_digits=8, decimal_places=2)

    tipo_apoyo = models.CharField(
        max_length=255,
        default='paep',
        choices=(
            ('paep', 'PAEP'),
            ('larga duración', 'Larga Duración'),
            ('otro', 'otro')    # ver como registrar otro
        ))

    tipo_actividad = models.CharField(
        max_length=25,
        default='estancia',
        choices=(
            ('curso', 'curso'),
            ('congreso', 'congreso'),
            ('estancia', 'estancia'),
            ('encuentro', 'encuentro'),
            ('otro', 'otro'),
        ))

    estado = models.CharField(
        max_length=25,
        default='solicitado',
        choices=(
            ('solicitado', 'solicitado'),
            ('reporte entregado', 'reporte entregado'),
            ('cancelado', 'cancelado'),
        ))

    notas = GenericRelation(Nota,
                            related_query_name='estancia')

    class Meta:
        verbose_name_plural = "Apoyos para Movilidad"

    def __str__(self):
        return u"%s | %s | %s" % (self.estudiante,
                                self.tipo_apoyo,
                                self.nombre)


class Sesion(models.Model):
    fecha = models.DateField()
    descripcion = models.CharField(max_length=100,
                                   default="sesión ordinaria")
    # guardar markdown
    minuta = models.TextField(blank=True)

    def as_a(self):
        return u"""<a href='%sinicio/sesiones/%s/'>%s %s</a>""" % (
            APP_PREFIX,
            self.id, self.fecha, self.descripcion)

    def __str__(self):
        return u'%s, %s' % (self.fecha,
                            self.descripcion)

    class Meta:
        verbose_name_plural = "Sesiones"


class Proyecto(models.Model):
    fecha = models.DateField()
    titulo = models.CharField(max_length=400)
    estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)

    plan = models.CharField(
        max_length=20,
        choices=[("Maestría", "Maestría"),
                 ("Doctorado", "Doctorado")],
        default="Maestría"
    )

    class Meta:
        ordering = ['-fecha', ]

    def __str__(self):
        return self.titulo


def anexo_path(instance, filename):
    (root, ext) = os.path.splitext(filename)
    return os.path.join(u'solicitudes/%s/%s' % (instance.solicitud.id,
                                                slugify(root) + ext))



def anexo_expediente_path(instance, filename):
    (root, ext) = os.path.splitext(filename)
    return os.path.join(u'expediente/%s/%s' % (instance.user.get_username(),
                                               slugify(root) + ext))


class AnexoExpediente(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)
    archivo = models.FileField(upload_to=anexo_expediente_path)

    def url(self):
        return u"%s/expediente/%s/%s" % (MEDIA_URL,
                                         self.user.get_username(),
                                         os.path.basename(self.archivo.path))

    def basename(self):
        return os.path.basename(self.archivo.file.name)

    def __str__(self):
        return self.basename()

    class Meta:
        verbose_name_plural = "Expedientes"


def anexo_academico_CV_path(instance, filename):
    (root, ext) = os.path.splitext(filename)
    return os.path.join(u'perfil-academico/%s/cv_%s' % (instance.id,
                                                        slugify(root) + ext))


def anexo_academico_solicitud_path(instance, filename):
    (root, ext) = os.path.splitext(filename)
    return os.path.join(
        u'perfil-academico/%s/solicitud_%s' % (instance.id,
                                               slugify(root) + ext))


def grado_path(instance, filename):
    (root, ext) = os.path.splitext(filename)
    return os.path.join(u'perfil-academico/%s/ultimo_grado_%s' % (
        instance.id,
        slugify(root) + ext))


def anexo_guia_path(instance, filename):
    (root, ext) = os.path.splitext(filename)
    return os.path.join(u'perfil-academico/%s/guia_avances_semestre_%s' % (
        instance.id,
        slugify(root) + ext))

def anexo_requisitos_comites_path(instance, filename):
    (root, ext) = os.path.splitext(filename)
    return os.path.join(u'perfil-academico/%s/requisitos_comites_%s' % (
        instance.id,
        slugify(root) + ext))


def anexo_vs_violencia_path(instance, filename):
    (root, ext) = os.path.splitext(filename)
    return os.path.join(u'perfil-academico/%s/vs_violencia_%s' % (
        instance.id,
        slugify(root) + ext))


class Academico(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    anexo_CV = models.FileField("CV en extenso",
                                upload_to=anexo_academico_CV_path,
                                blank=True, null=True)

    ultimo_grado = models.FileField("Copia de último grado académico",
                                    upload_to=grado_path,
                                    blank=True, null=True)

    anexo_solicitud = models.FileField(
        "Carta de solicitud de acreditación o reacreditación como tutor(a)",
        upload_to=anexo_academico_solicitud_path,
        blank=True, null=True)


    anexo_guia_avances = models.FileField("Guía de avances por semestre para maestría y doctorado",
                                          upload_to=anexo_guia_path,
                                          blank=True, null=True)


    anexo_requisitos_comites = models.FileField("Criterios para conformación de comités tutores",
                                                upload_to=anexo_requisitos_comites_path,
                                                blank=True, null=True)

    anexo_vs_violencia = models.FileField("Compromiso por una academia libre de violencias",
                                          upload_to=anexo_vs_violencia_path,
                                          blank=True, null=True)

    estimulo_UNAM = models.CharField(u"Estímulo UNAM",
                                     max_length=15,
                                     default='ninguno',
                                     choices=(('ninguno', 'ninguno'),
                                              ('Equivalencia', 'Equivalencia'),
                                              ('PEPASIG', 'PEPASIG'),
                                              ('PEI', 'PEI'),
                                              ('PEDMETI', 'PEDMETI'),
                                              ('PRIDE A', 'PRIDE A'),
                                              ('PRIDE B', 'PRIDE B'),
                                              ('PRIDE C', 'PRIDE C'),
                                              ('PRIDE D', 'PRIDE D')))

    nivel_SNI = models.CharField(u"Nivel SNI",
                                 max_length=15,
                                 default='sin SNI',
                                 blank=True, null=True,
                                 choices=(('sin SNI', 'sin SNI'),
                                          ('I', 'I'),
                                          ('II', 'II'),
                                          ('III', 'III'),
                                          ('C', 'C'),
                                          ('E', 'E')))

    CVU = models.CharField(u"Número de CVU",
                           max_length=100, blank=True, null=True,
                           help_text="movido a Perfil Personal")

    numero_trabajador_unam = models.CharField(u"Número de trabajador (UNAM)",
                                              max_length=100,
                                              blank=True, null=True)

    comite_academico = models.BooleanField(default=False)

    fecha_acreditacion = models.DateField(blank=True, null=True, default=datetime.date.today)

    acreditacion = models.CharField(
        max_length=25,
        choices=(
            ('candidato', 'candidato'),
            ('candidato profesor', 'candidato profesor'),
            ('no acreditado', 'no acreditado'),
            (u'información incompleta', u'información incompleta'),
            ('por reacreditar D', 'por reacreditar D'),
            ('por reacreditar M', 'por reacreditar M'),
            ('baja', 'baja'),
            ('condicionado', 'condicionado'),
            ('P', 'P'),
            ('D', 'D'),
            ('M', 'M'),
            ('MCT_M', 'MCT_M'),
            ('E', 'E')
        ))

    adscripcion_pcs = models.ForeignKey(EntidadPCS,
                                        related_name="academicos_adscritos",
                                        blank=True, null=True,
                                        on_delete=models.CASCADE,
                                        verbose_name="Adscripción PCS")


    asociacion_pcs = models.ForeignKey(EntidadPCS,
                                       related_name='academicos_asociados',
                                       blank=True, null=True,
                                       on_delete=models.CASCADE,
                                       verbose_name="Asociación PCS")



    campos_de_conocimiento = models.ManyToManyField(
        CampoConocimiento,
        blank=True)

    lineas_de_investigacion = models.ManyToManyField(
        LineaInvestigacion,
        blank=True)





    # Resumen Curricular

    # formacion de estudiantes
    tesis_doctorado = models.PositiveSmallIntegerField(
        u"""Cantidad total de participaciones como tutor principal de
        estudiantes graduados de nivel Doctorado""",
        null=True, blank=True)
    tesis_doctorado_5 = models.PositiveSmallIntegerField(
        u"""Cantidad de participaciones como tutor principal de
        estudiantes graduados de nivel Doctorado en los últimos 5 años""",
        null=True, blank=True)

    tesis_maestria = models.PositiveSmallIntegerField(
        u"""Cantidad total participaciones como tutor principal de estudiantes
        graduados de nivel Maestría""",
        null=True, blank=True)
    tesis_maestria_5 = models.PositiveSmallIntegerField(
        u"""Cantidad de participaciones como tutor principal de estudiantes
           graduados de nivel Maestría en los últimos 5 años""",
        null=True, blank=True)

    tesis_licenciatura = models.PositiveSmallIntegerField(
        u"""Cantidad total de participaciones como tutor principal de
        estudiantes graduados de nivel Licenciatura""",
        null=True, blank=True)
    tesis_licenciatura_5 = models.PositiveSmallIntegerField(
        u"""Cantidad de participaciones como tutor principal de
        estudiantes " graduados a nivel Licenciatura en los últimos 5
        años""", null=True, blank=True)

    tutor_principal_otros_programas = models.TextField(
        u"""Nombres de los otros programas de posgrado en los que participa""",
        blank=True)

    comite_doctorado_otros = models.PositiveSmallIntegerField(
        u"""Cantidad total de participaciones como miembro de comité
        tutor (no tutor principal) de estudiantes graduados de nivel
        doctorado.""", null=True, blank=True)

    comite_maestria_otros = models.PositiveSmallIntegerField(
        u"""Cantidad total de participaciones como miembro de comité
        tutor (no tutor principal) de estudiantes graduados de nivel
        maestría.""", null=True, blank=True)

    # en el PCS
    participacion_tutor_doctorado = models.PositiveSmallIntegerField(
        u"""Cantidad de participaciones como tutor principal en el Posgrado en
           Ciencias de la Sostenibilidad a nivel doctorado""",
        null=True, blank=True)

    participacion_tutor_maestria = models.PositiveSmallIntegerField(
        u"""Cantidad de participaciones como tutor principal en el
        Posgrado en Ciencias de la Sostenibilidad a nivel maestría""",
        null=True, blank=True)

    participacion_comite_doctorado = models.PositiveSmallIntegerField(
        u"""Cantidad de participaciones como miembro de comité tutor (no tutor
        principal) en el Posgrado en Ciencias de la Sostenibilidad a
        nivel doctorado""",
        null=True, blank=True)

    participacion_comite_maestria = models.PositiveSmallIntegerField(
        u"""Cantidad de participaciones como miembro de comité tutor (no tutor
         principal) en el Posgrado en Ciencias de la Sostenibilidad a
         nivel maestría""",
        null=True, blank=True)

    otras_actividades = models.TextField(
        u"""Si no cuenta con estudiantes graduados, indique si cuenta con
        otras actividades académicas como estancias de investigación,
        seminarios de titulación, etc.""", blank=True, null=True)

    # publicaciones
    articulos_internacionales = models.PositiveSmallIntegerField(
        u"Cantidad total de artículos publicados en revistas internacionales",
        null=True, blank=True)

    articulos_internacionales_5 = models.PositiveSmallIntegerField(
        u"Cantidad de artículos publicados en revistas internacionales "
        + u"durante los últimos 5 años",
        null=True, blank=True)

    articulos_nacionales = models.PositiveSmallIntegerField(
        u"Cantidad total de artículos publicados en revistas nacionales",
        null=True, blank=True)

    articulos_nacionales_5 = models.PositiveSmallIntegerField(
        u"Cantidad de artículos publicados en revistas nacionales "
        + u"durante los últimos 5 años",
        null=True, blank=True)

    libros = models.PositiveSmallIntegerField(
        u"Cantidad total de libros publicados",
        null=True, blank=True)

    libros_5 = models.PositiveSmallIntegerField(
        u"Cantidad de libros publicados durante los últimos 5 años",
        null=True, blank=True)

    capitulos = models.PositiveSmallIntegerField(
        u"Cantidad total de capítulos de libro publicados",
        null=True, blank=True)

    capitulos_5 = models.PositiveSmallIntegerField(
        u"Cantidad de capítulos de libro publicados durante los últimos 5 años",
        null=True, blank=True)

    top_5 = models.TextField(
        u"""Cinco publicaciones más destacadas en temas relacionados con las
        Ciencias de la Sostenibilidad.  De ser posible incluir ligas
        para acceder a las publicaciones.""",
        blank=True)

    otras_publicaciones = models.TextField(
        u"""Si su productividad académica no se ve reflejada en las
        publicaciones anteriores, indique si cuenta con otros
        productos como por ejemplo informes técnicos, memorias
        técnicas, desarrollo de proyectos hasta nivel ejecutivo,
        planes y programas de desarrollo urbano, manuales, etc.""",
        null=True, blank=True
    )

    # Actividad profesional y de Investigación
    lineas = models.TextField(
        u"Temas de interés y/o experiencia en ciencias de la sostenibilidad, "
        + u"máximo 10, uno por renglón",
        blank=True)

    palabras_clave = models.TextField(
        u"Palabras clave de temas de interés y/o experiencia"
        + u"en ciencias de la sostenibilidad, máximo 10, una por renglón",
        blank=True)
    motivacion = models.TextField(
        u"Motivación para participar en el Programa, máximo 200 palabras",
        blank=True)
    proyectos_sostenibilidad = models.TextField(
        u"Principales proyectos relacionados con "
        + u"ciencias de la sostenibilidad durante los últimos cinco años, "
        + u"especificar si se es responsable o colaborador.",
        blank=True)
    proyectos_vigentes = models.TextField(
        u"Proyectos vigentes en los que pueden "
        + u"participar alumnos del PCS. Incluya fechas de terminación.",
        blank=True)

    # disponibilidad
    disponible_tutor = models.BooleanField(
        u"Disponible como tutor principal (dirección de alumnos)",
        default=True)

    disponible_miembro = models.BooleanField(
        u"Disponible como miembro de comité tutor (asesoría de alumnos)",
        default=True)

    # epílogo
    observaciones = models.TextField(blank=True)

    resumen_completo = models.BooleanField(u"resumen curricular",
                                           default=False)

    perfil_personal_completo = models.BooleanField(default=False)

    anexos_completo = models.BooleanField(default=False)

    semaforo_MCT_M = models.CharField(
        max_length=10, default="rojo",
        choices=(
            ('amarillo', 'amarillo'),
            ('rojo', 'rojo')))
    
    semaforo_maestria = models.CharField(
        max_length=10, default="rojo",
        choices=(
            ('verde', 'verde'),
            ('amarillo', 'amarillo'),
            ('rojo', 'rojo')))

    semaforo_doctorado = models.CharField(
        max_length=10, default="rojo",
        choices=(
            ('verde', 'verde'),
            ('amarillo', 'amarillo'),
            ('rojo', 'rojo')))

    titulo_honorifico = models.CharField(u'Título honorífico (Dra., Mtro, Lic)',
        max_length=12, default='Lic.',
        null=True, blank=True,
        choices=(
            (u'Lic.',  u'Lic.'),
            (u'Mtro.', u'Mtro.'),
            (u'Mtra.', u'Mtra.'),
            (u'Dr.',   u'Dr.'),
            (u'Dra.',  u'Dra.')))

    notas = GenericRelation(Nota,
                            related_query_name='academico')


    @property
    def perfil_profesor_completo(self):
        if self.ultimo_grado and self.anexo_CV:
            return True
        else:
            return False


    def estudiantes(self, plan='ambos'):
        """
        lista de estudiantes que con cualquier tipo de membresía
        estuvieron bajo el ala de este académico
        """
        estudiantes = set()

        if plan == 'ambos':
            for m in self.tutorias.all().order_by('tipo'):
                estudiantes.add(m.estudiante)
        else:
            if plan == 'Doctorado':
                suffix = 'D'
            elif plan == 'Maestría':
                suffix = 'M'

            for m in self.tutorias.filter(tipo__endswith=suffix):
                estudiantes.add(m.estudiante)

        return estudiantes



    def tutorias_estudiantes(self, plan):
        membresias = []
        for e in self.estudiantes(plan=plan):
            for m in e.get_comite(plan, con_invitados=False):
                if m.tutor == self:
                    membresias.append(m)
        return membresias


    def tutorias_conteo_estados(self):
        t = {}
        for tipo, tipo_label in tipos_membresia:
            t[(tipo, 'indeterminado')] = 0
            for estado, estado_label in estados_estudiante:
                if tipo.endswith('M') and estado.endswith('maestría'):
                    t[(tipo, estado)] = 0

                if tipo.endswith('D') and estado.endswith('doctorado'):
                    t[(tipo, estado)] = 0

        for plan in ['Maestría', 'Doctorado']:
            for m in self.tutorias_estudiantes(plan):
                t[(m.tipo, m.estudiante.desenlace(plan))] += 1

        return t


    @property
    def disponible_tutor_vobo(self):
        estudiantes = 0
        conteo = self.tutorias_conteo_estados()
        for t in conteo:
            tipo, estado = t
            if ((tipo.startswith('TP') or tipo.startswith('C'))
                and
                (estado.startswith('inscrito') or estado.startswith('egresado'))):
                estudiantes += conteo[t]

        if estudiantes >= 3:
            return False
        else:
            return True


    @property
    def disponible_miembro_vobo(self):
        estudiantes = 0
        conteo = self.tutorias_conteo_estados()
        for t in conteo:
            tipo, estado = t
            if (tipo.startswith('MC')
                and
                (estado.startswith('inscrito') or estado.startswith('egresado'))):
                estudiantes += conteo[t]

        if estudiantes >= 5:
            return False
        else:
            return True





    def verifica_titulo_honorifico(self):
        if self.is_phd():
            if self.user.perfil.genero == 'M':
                self.titulo_honorifico = 'Dr.'
            else:
                self.titulo_honorifico = 'Dra.'

        elif self.is_msc():
            if self.user.perfil.genero == 'M':
                self.titulo_honorifico = u'Mtro.'
            else:
                self.titulo_honorifico = u'Mtra.'
        else:
            self.titulo_honorifico = u'Lic.'


    def copia_ultima_acreditacion(self):
        if self.acreditaciones.count() > 0:
            ultima = self.acreditaciones.latest('fecha')
            self.acreditacion = ultima.acreditacion
            self.fecha_acreditacion = ultima.fecha


    def is_phd(self):
        niveles = [deg.nivel
                   for deg in self.user.gradoacademico_set.all()]
        niveles = set(niveles)
        if 'doctorado' in niveles:
            return True
        else:
            return False

    def is_msc(self):
        niveles = [deg.nivel
                   for deg in self.user.gradoacademico_set.all()]
        niveles = set(niveles)
        if 'maestria' in niveles:
            return True
        else:
            return False

    def show_acreditacion(self):
        if self.acreditacion == 'no acreditado':
            return 'no acreditado'
        elif self.acreditacion == 'E':
            return u"Comité tutor específico"
        elif (self.acreditacion == 'D'):
            return "Maestría y Doctorado (dirección y co-dirección de maestría y doctorado y participación como miembro de comités tutores de maestría y doctorado)."
        elif self.acreditacion == "M":
            return "Maestría y Miembro de Comité Tutor de Doctorado (dirección y co-dirección de maestría y participación como miembro de comités tutores de maestría y doctorado)."
        elif self.acreditacion == "MCT_M":
            return "Miembro de Comité Tutor de Maestría (participación como miembro de comités tutores de maestría)."
        else:
            return self.acreditacion


    def tiene_perfil_personal(self):
        if hasattr(self.user, 'perfil'):
            return True
        else:
            return False

    def tiene_grados(self):
        if self.user.gradoacademico_set.count() > 0:
            return True
        else:
            return False

    def verifica_anexos_completo(self):
        """ True si el perfil academico tiene seis anexos """

        if not self.ultimo_grado:
            return False

        if not self.anexo_CV:
            return False

        if not self.anexo_solicitud:
            return False

        if not self.anexo_guia_avances:
            return False

        if not self.anexo_requisitos_comites:
            return False

        if not self.anexo_vs_violencia:
            return False

        return True


    def actualiza_anexos_completo(self):
        self.anexos_completo = self.verifica_anexos_completo()
        self.save()

    def verifica_perfil_personal_completo(self):
        if (self.tiene_perfil_personal()
            and self.tiene_grados()
            and self.user.perfil.adscripcion_ok()):
            return True
        else:
            return False


    def actualiza_perfil_personal_completo(self):
        self.perfil_personal_completo = self.verifica_perfil_personal_completo()
        self.save()


    def carencias_general(self):
        carencias = u""

        if self.CVU == "":
            carencias += u" - " + str(self._meta.get_field('CVU').verbose_name) + u"\n"

        if self.anexo_CV == "":
            carencias += u" - " + str(self._meta.get_field('anexo_CV').verbose_name) + u"\n"

        if self.anexo_solicitud == "":
            carencias += u" - " + str(self._meta.get_field('anexo_solicitud').verbose_name) + u"\n"

        if not self.ultimo_grado:
            carencias += u" - " + str(self._meta.get_field('ultimo_grado').verbose_name) + u"\n"

        return carencias


    def carencias_actividad(self):
        carencias = u""

        if self.lineas == "" or self.lineas is None:
            carencias += u" - " + str(self._meta.get_field('lineas').verbose_name) + u"\n"

        if self.palabras_clave == "" or self.palabras_clave is None:
            carencias += u" - " + str(self._meta.get_field('palabras_clave').verbose_name) + u"\n"

        if self.motivacion == "" or self.motivacion is None:
            carencias += u" - " + str(self._meta.get_field('motivacion').verbose_name) + u"\n"

        return carencias


    def as_a(self):
        icon = """<span class='glyphicon glyphicon-{icon}'
                        aria-hidden=true></span>"""
        icon.format(icon='thumbs-up')

        return u"""<a href='%sinicio/usuario/%s/'>%s %s</a>""" % (
            APP_PREFIX,
            self.user.id, icon, self.__str__())

    def perfil_publico_anchor(self):
        return u"""<a href='%sinicio/perfil/publico/%s'>%s</a>""" % (
            APP_PREFIX,
            self.user.get_username(), self.__str__())

    def perfil_comite_anchor(self):
        return u"""<a href='%sinicio/perfil/comite/%s'>🗫</a>""" % (
            APP_PREFIX,
            self.user.get_username())

    def nombre_completo(self):
        return self.__str__()

    def __str__(self):
        name = self.user.get_full_name()
        if not name:
            name = self.user.username

        return "%s" % name

    def publicaciones_5(self):
        try:
            return sum((self.articulos_internacionales_5,
                        self.articulos_nacionales_5,
                        self.libros_5,
                        self.capitulos_5))
        except TypeError:
            return 0


    def verifica_resumen_completo(self):
        """
        verdadero si hay ResumenCurricular con data en los campos oblitagorios
        """
        if not hasattr(self, 'resumen_curricular'):
            return False

        if not self.resumen_curricular.experiencia_docente:
            return False

        if not self.resumen_curricular.publicaciones:
            return False

        if not self.resumen_curricular.palabras_clave:
            return False

        if not self.resumen_curricular.metodologias:
            return False

        if not self.resumen_curricular.marcos:
            return False

        if not self.resumen_curricular.motivacion:
            return False

        if not self.resumen_curricular.proyectos:
            return False

        return True


    def actualiza_resumen_completo(self):
        self.resumen_completo = self.verifica_resumen_completo()


        
    def verifica_semaforo_MCT_M(self):
        if not hasattr(self, 'resumen_curricular'):
            return 'rojo'
        
        publicaciones = (
            self.resumen_curricular.articulos_recientes
            +
            self.resumen_curricular.capitulos_recientes
            +
            self.resumen_curricular.libros_recientes)

        tutor_lic = self.resumen_curricular.tutor_licenciatura >= 1
        miembro_maestria = self.resumen_curricular.miembro_maestria >= 2

        if (publicaciones >= 3 and (tutor_lic or miembro_maestria)):
            return 'verde'
        elif (publicaciones >= 3):
            return 'amarillo'
        else:
            return 'rojo'


    def verifica_semaforo_maestria(self):
        if not hasattr(self, 'resumen_curricular'):
            return 'rojo'
        
        publicaciones = (
            self.resumen_curricular.articulos_recientes
            +
            self.resumen_curricular.capitulos_recientes
            +
            self.resumen_curricular.libros_recientes) >= 3

        tutor_lic = self.resumen_curricular.tutor_licenciatura >= 1
        miembro_maestria = self.resumen_curricular.miembro_maestria >= 2

        if (publicaciones and (tutor_lic or miembro_maestria)):
            return 'amarillo'
        else:
            return 'rojo'

        
    def verifica_semaforo_doctorado(self):
        if not hasattr(self, 'resumen_curricular'):
            return 'rojo'
        
        tutor_maestria = self.resumen_curricular.tutor_maestria >= 1
        tutor_doctorado= self.resumen_curricular.tutor_doctorado >= 1
        miembro_doctorado = self.resumen_curricular.miembro_doctorado >= 2

        publicaciones = (
            self.resumen_curricular.articulos_recientes
            +
            self.resumen_curricular.capitulos_recientes
            +
            self.resumen_curricular.libros_recientes) >= 5

        if ((tutor_maestria
             or
             tutor_doctorado
             or miembro_doctorado)
            and publicaciones):
            return 'amarillo'
        else:
            return 'rojo'
        

    def wc_resumen_academico(self):
        """ word cloud """

        if not self.verifica_resumen_completo():
            return None

        wordcloud = WordCloud()

        wordcloud.background_color = '#fffbeb'
        
        with open(BASE_DIR + '/posgradmin/stopwords-es.txt', 'r',
                  encoding="utf-8") as f:
            for w in f.readlines():
                wordcloud.stopwords.add(w.strip())

        for w in self.user.get_full_name().split(' '):
            wordcloud.stopwords.add(w)

        wordcloud.stopwords.add('doi')
        wordcloud.stopwords.add('None')
        wordcloud.stopwords.add('http')
        wordcloud.stopwords.add('https')

        text = " ".join([self.resumen_curricular.experiencia_docente,
                         self.resumen_curricular.publicaciones,
                         self.resumen_curricular.otros_productos,
                         self.resumen_curricular.palabras_clave,
                         self.resumen_curricular.metodologias,
                         self.resumen_curricular.motivacion,
                         self.resumen_curricular.proyectos])

        if not text.strip():
            return None

        wordcloud.generate(text)

        fig = plt.figure(figsize=(8, 6), dpi=150)
        plt.imshow(wordcloud, interpolation='bilinear')
        plt.axis('off')

        pc_path = '%s/perfil-academico/%s/' % (MEDIA_ROOT, self.id)
        if not os.path.isdir(pc_path):
            os.makedirs(pc_path)

        fig.tight_layout()
        plt.savefig(f'{MEDIA_ROOT}/perfil-academico/{self.id}/wordcloud.svg',
                    transparent=True)

        return '%s/perfil-academico/%s/wordcloud.png' % (MEDIA_ROOT, self.id)


    def pc_resumen_academico(self):
        """ parallel coordinates del resumen academico """

        if not self.verifica_resumen_completo():
            return None

        escala_sni = {'sin SNI': 0,
                      'I': 1,
                      'II': 2,
                      'III': 3,
                      'C': 4,
                      'E': 5}
        escala_estimulo = {
            'ninguno': 0,
            'Equivalencia': 1,
            'PEPASIG': 2,
            'PEI': 3,
            'PEDMETI': 4,
            'PRIDE A': 5,
            'PRIDE B': 6,
            'PRIDE C': 7,
            'PRIDE D': 8}


        avg_SNI = np.mean([escala_sni.get(a.nivel_SNI, 0)
                           for a in Academico.objects.all()])
        avg_estimulo_UNAM = np.mean([escala_estimulo.get(a.estimulo_UNAM, 0)
                                     for a in Academico.objects.all()])
        avg_licenciatura = np.mean([a.tesis_licenciatura
                                    for a in Academico.objects.filter(tesis_licenciatura__gt=0)])
        avg_licenciatura_5 = np.mean([a.tesis_licenciatura_5
                                      for a in Academico.objects.filter(tesis_licenciatura_5__gt=0)])
        avg_maestria = np.mean([a.tesis_maestria
                                for a in Academico.objects.filter(tesis_maestria__gt=0)])

        avg_maestria_5 = np.mean([a.tesis_maestria_5
                                  for a in Academico.objects.filter(tesis_maestria_5__gt=0)])
        avg_doctorado = np.mean([a.tesis_doctorado
                                 for a in Academico.objects.filter(tesis_doctorado__gt=0)])
        avg_doctorado_5 = np.mean([a.tesis_doctorado_5
                                   for a in Academico.objects.filter(tesis_doctorado_5__gt=0)])
        avg_comite_doctorado_otros = np.mean([a.comite_doctorado_otros
                                              for a in Academico.objects.filter(comite_doctorado_otros__gt=0)])
        avg_comite_maestria_otros = np.mean([a.comite_maestria_otros
                                             for a in Academico.objects.filter(comite_maestria_otros__gt=0)])
        avg_participacion_comite_maestria = np.mean([a.participacion_comite_maestria
                                                 for a in Academico.objects.filter(participacion_comite_maestria__gt=0)])
        avg_participacion_tutor_maestria = np.mean([a.participacion_tutor_maestria
                                                    for a in Academico.objects.filter(participacion_tutor_maestria__gt=0)])
        avg_participacion_comite_doctorado = np.mean([a.participacion_comite_doctorado
                                                      for a in Academico.objects.filter(participacion_comite_doctorado__gt=0)])

        avg_participacion_tutor_doctorado = np.mean([a.participacion_tutor_doctorado
                                                     for a in Academico.objects.filter(participacion_tutor_doctorado__gt=0)])
        avg_articulos_internacionales_5 = np.mean([a.articulos_internacionales_5
                                                   for a in Academico.objects.filter(articulos_internacionales_5__gt=0)])
        avg_articulos_nacionales_5 = np.mean([a.articulos_nacionales_5
                                              for a in Academico.objects.filter(articulos_nacionales_5__gt=0)])

        avg_articulos_internacionales = np.mean([a.articulos_internacionales
                                                 for a in Academico.objects.filter(articulos_internacionales__gt=0)])
        avg_articulos_nacionales = np.mean([a.articulos_nacionales
                                            for a in Academico.objects.filter(articulos_nacionales__gt=0)])
        avg_capitulos = np.mean([a.capitulos
                                 for a in Academico.objects.filter(capitulos__gt=0)])
        avg_capitulos_5 = np.mean([a.capitulos_5
                                   for a in Academico.objects.filter(capitulos_5__gt=0)])
        avg_libros = np.mean([a.libros
                              for a in Academico.objects.filter(libros__gt=0)])
        avg_libros_5 = np.mean([a.libros_5
                                for a in Academico.objects.filter(libros_5__gt=0)])
        avg_gradoacademico = np.mean([a.user.gradoacademico_set.count()
                                      for a in Academico.objects.all()])



        df = pd.DataFrame({
            u'académico': [self.user.get_full_name(), 'avg'],
            u'SNI': [escala_sni.get(self.nivel_SNI, 0), avg_SNI, ],
            u'estímulo UNAM': [escala_estimulo.get(self.estimulo_UNAM, 0), avg_estimulo_UNAM],
            u"licenciatura": [self.tesis_licenciatura, avg_licenciatura],
            u"licenciatura últimos 5 años": [
                self.tesis_licenciatura_5, avg_licenciatura_5],
            u"maestría": [self.tesis_maestria, avg_maestria],
            u"maestría 5": [self.tesis_maestria_5, avg_maestria_5],
            u"doctorado": [self.tesis_doctorado, avg_doctorado],
            u"doctorado 5": [self.tesis_doctorado_5, avg_doctorado_5],
            u"comité doctorado otros programas": [
                self.comite_doctorado_otros, avg_comite_doctorado_otros],
            u"comité maestría otros programas": [
                self.comite_maestria_otros, avg_comite_maestria_otros],
            u"participación comite maestría": [
                self.participacion_comite_maestria, avg_participacion_comite_maestria],
            u"participación tutor maestría": [
                self.participacion_tutor_maestria, avg_participacion_tutor_maestria],
            u"participación comite doctorado": [
                self.participacion_comite_doctorado, avg_participacion_comite_doctorado],
            u"participación tutor doctorado": [
                int(self.participacion_tutor_doctorado) if self.participacion_tutor_doctorado is not None else 0,
                avg_participacion_tutor_doctorado],
            u"artículos internacionales últimos 5 años": [
                self.articulos_internacionales_5, avg_articulos_internacionales_5],
            u"artículos nacionales últimos 5 años": [
                self.articulos_nacionales_5, avg_articulos_nacionales_5],
            u"artículos internacionales": [self.articulos_internacionales, avg_articulos_internacionales],
            u"artículos nacionales": [self.articulos_nacionales, avg_articulos_nacionales],
            u"capítulos": [self.capitulos, avg_capitulos],
            u"capítulos últimos 5 años": [self.capitulos_5, avg_capitulos_5],
            u"libros": [self.libros, avg_libros],
            u"libros últimos 5 años": [self.libros_5, avg_libros_5],
            u'grados académicos': [self.user.gradoacademico_set.count(), avg_gradoacademico],
            },
            columns=[u'académico',
                     u'SNI',
                     u'estímulo UNAM',
                     u"licenciatura",
                     u"licenciatura últimos 5 años",
                     u"maestría",
                     u"maestría 5",
                     u"doctorado",
                     u"doctorado 5",
                     u"comité doctorado otros programas",
                     u"comité maestría otros programas",
                     u"participación comite maestría",
                     u"participación tutor maestría",
                     u"participación comite doctorado",
                     u"participación tutor doctorado",
                     u"artículos internacionales últimos 5 años",
                     u"artículos nacionales últimos 5 años",
                     u"artículos internacionales",
                     u"artículos nacionales",
                     u"capítulos",
                     u"capítulos últimos 5 años",
                     u"libros",
                     u"libros últimos 5 años",
                     u'grados académicos']
        )

        fig, ax = plt.subplots()
        # rectangulo estudiantes
        max_grad = max([
            df[u"licenciatura"][0],
            df[u"maestría"][0],
            df[u"doctorado"][0],
            ])
        ax.add_patch(
            patches.Rectangle(
                (3, 0), 5, max_grad,
                color='orchid', alpha=0.25, linewidth=0))

        # rectangulo para publicaciones
        max_pub = max([
            df[u'capítulos'][0],
            df[u'libros'][0],
            df[u"artículos nacionales"][0],
            df[u"artículos internacionales"][0],
        ])
        ax.add_patch(
            patches.Rectangle(
                (15, 0), 7, max_pub,
                color='deepskyblue', alpha=0.25, linewidth=0))

        parallel_coordinates(df, u'académico', color=['deeppink', 'grey'])

        legend = ax.legend()
        legend.remove()

        plt.xticks(rotation=90)

        # plt.ylim(0, 150)

        fig.tight_layout()
        pc_path = '%s/perfil-academico/%s/' % (MEDIA_ROOT, self.id)
        if not os.path.isdir(pc_path):
            os.makedirs(pc_path)
        plt.savefig(
            '%s/perfil-academico/%s/pc_resumen_academico.png'
            % (MEDIA_ROOT, self.id))


    class Meta:
        verbose_name_plural = "Académicos"
        ordering = ['user__first_name', 'user__last_name', ]




class ResumenCurricular(models.Model):

    academico = models.OneToOneField(Academico,
                                     null=True, blank=True,
                                     related_name="resumen_curricular",
                                     on_delete=models.CASCADE)

    """
    Formación de Estudiantes (trayectoria profesional completa)

    • Cantidad total de participaciones como tutor(a) principal o co-tutor(a) de estudiantado
      graduado a nivel:
    """
    tutor_doctorado = models.PositiveSmallIntegerField("¿Cuántas veces ha sido tutor(a) principal o co-tutor(a) de estudiantes graduados a nivel doctorado en este u otros programas de posgrado?",
                                                       default=0)
    tutor_maestria = models.PositiveSmallIntegerField("¿Cuántas veces ha sido tutor(a) principal o co-tutor(a) de estudiantes graduados a nivel maestría en este u otros programas de posgrado?",
                                                      default=0)
    tutor_licenciatura = models.PositiveSmallIntegerField("¿Cuántas veces ha sido tutor(a) principal o co-tutor(a) de estudiantes graduados a nivel licenciatura?",
                                                          default=0)

    """
    • Cantidad total de participaciones como miembro de comité tutor
      (excluyendo co-tutoría o tutoría principal) de estudiantado
      graduado a nivel
    """
    miembro_doctorado = models.PositiveSmallIntegerField("¿Cuántas veces ha participado como miembro de comité tutor de estudiantes graduados a nivel doctorado? excluyendo tutorías principales y co-tutorías",
                                                         default=0)
    miembro_maestria = models.PositiveSmallIntegerField("¿Cuántas veces ha participado como miembro de comité tutor de estudiantes graduados a nivel maestría? excluyendo tutorías principales y co-tutorías",
                                                        default=0)

    otros_posgrados = models.TextField("¿En qué otros posgrados participa?",
                                       blank=True,
                                       help_text="Nombres de los otros programas de posgrado en los que participa")


    """
    Experiencia docente

    """
    experiencia_docente = models.TextField(
        help_text="""Enlistar cursos, talleres o seminarios que ha impartido en los
        últimos cinco años (nombre, nivel y duración de cada uno), máximo 10,
        uno por renglón.""")


    ###"""Publicaciones """

    publicaciones = models.TextField(
        help_text="""Enlistar cinco publicaciones relevantes en temas sólo de
        ciencias de la sostenibilidad (incluir DOI o ligas directas).""")

    otros_productos = models.TextField(
        blank=True,
        help_text="""Enlistar otros productos, máximo 10, como informes técnicos, memorias
        técnicas, desarrollo de proyectos hasta nivel ejecutivo, planes
        y programas de desarrollo urbano, manuales, que sean relevantes
        para la sostenibilidad.""")


    ###"""Total de publicaciones (trayectoria profesional completa)"""
    articulos_total = models.PositiveSmallIntegerField("Total de artículos",
        default=0,
        help_text="Cantidad total de artículos publicados en revistas (nacionales e internacionales)")
    articulos_recientes = models.PositiveSmallIntegerField("Artículos recientes",
        default=0,
        help_text="Cantidad de artículos publicados en revistas (nacionales e internacionales) durante los últimos 5 años")

    capitulos_total = models.PositiveSmallIntegerField("Capítulos de libro",
        default=0,
        help_text="Cantidad total de capítulos de libro publicados")

    capitulos_recientes = models.PositiveSmallIntegerField("Capítulos de libro recientes",
        default=0,
        help_text="Cantidad de capítulos de libro publicados durante los últimos 5 años")

    libros_total = models.PositiveSmallIntegerField("Libros",
        default=0,
        help_text="Cantidad total de libros publicados")
    libros_recientes = models.PositiveSmallIntegerField("Libros recientes",
        default=0,
        help_text="Cantidad de libros publicados durante los últimos 5 años")


    ####
    # Actividad profesional y de Investigación
    ####

    palabras_clave = models.TextField(
        help_text="""Enlistar palabras clave de temas de interés y/o experiencia en
        ciencias de la sostenibilidad, máximo 10, una por renglón""")

    metodologias = models.TextField(
        help_text="""Enlistar metodologías que aplica en sus investigaciones, máximo
        10, una por renglón""")

    marcos = models.TextField("Marcos conceptuales",
        help_text="""Enlistar marcos conceptuales que aplica en sus investigaciones,
      máximo 10, una por renglón""")

    motivacion = models.TextField("Motivación para participar en el PCS",
        help_text="""En máximo 1000 palabras, explicar el vínculo
      entre su formación académica y su experiencia profesional con
      los objetivos del PCS; detallar conocimiento/experiencia con
      investigaciones multi, inter y transdisciplinarias; señalar qué
      aportes para la sostenibilidad ofrece para la formación del
      estudiantado""")

    proyectos = models.TextField(
        help_text="""
        Proyectos vigentes o convenios de colaboración en los que pueden
        participar alumnos del PCS. Señalar si ofrece financiamiento
        para trabajo de campo, laboratorio, término de tesis y si ofrece
        espacio físico para el estudiantado. Incluya fechas de
        terminación
        """)

    class Meta:
        verbose_name_plural = "Resúmenes curriculares"


    def __str__(self):
        return str(self.academico)



class Acreditacion(models.Model):

    academico = models.ForeignKey(Academico, related_name="acreditaciones", on_delete=models.CASCADE)
    fecha = models.DateField(default=datetime.date.today)
    comentario = models.CharField(max_length=400,
                                  null=True, blank=True)
    acreditacion = models.CharField(
        max_length=25,
        default='candidato',
        choices=(
            ('candidato', 'candidato'),
            ('candidato profesor', 'candidato profesor'),
            ('no acreditado', 'no acreditado'),
            (u'información incompleta', u'información incompleta'),
            ('por reacreditar D', 'por reacreditar D'),
            ('por reacreditar M', 'por reacreditar M'),
            ('baja', 'baja'),
            ('condicionado', 'condicionado'),
            ('P', 'P'),
            ('D', 'D'),
            ('M', 'M'),
            ('MCT_M', 'MCT_M'),
            ('E', 'E')
        ))

    def __str__(self):
        return self.acreditacion

    class Meta:
        verbose_name_plural = "acreditaciones"
        ordering = ['fecha', ]


    def genera_carta(self):
        if self.acreditacion in ('D', 'M', 'MCT_M'):

            if self.academico.user.gradoacademico_set.filter(nivel='doctorado').count() > 0:
                if self.academico.user.perfil.genero == 'M':
                    dr = 'Dr. '
                else:
                    dr = 'Dra. '
            else:
                dr = ""

            outdir = '%s/expediente/%s/' % (MEDIA_ROOT, self.academico.user.username)
            pathlib.Path(outdir).mkdir(parents=True, exist_ok=True)

            tmpname = 'cartaplain_%s.pdf' % self.acreditacion

            with open(outdir + tmpname + '.md', 'w', encoding='utf8') as f:
                f.write(
                    render_to_string('posgradmin/carta_acreditacion.md',
                                     {'fecha': datetime.date.today(),
                                      'ac': self,
                                      'dr': dr,
                                      'firma': BASE_DIR + '/docs/firma.png'}))

            os.chdir(outdir)
            subprocess.run(["pandoc", outdir + tmpname + '.md',
                            "--to", "latex",
                            "--output", outdir + tmpname])

            C = PdfReader(outdir + tmpname)
            M = PdfReader(BASE_DIR + '/docs/membrete_pcs.pdf')
            w = PdfWriter()
            merger = PageMerge(M.pages[0])
            merger.add(C.pages[0]).render()

            pathlib.Path(outdir + tmpname).unlink()
            pathlib.Path(outdir + tmpname + '.md').unlink()

            final_name = tmpname.replace('cartaplain', 'carta_acreditacion')
            w.write(outdir + final_name, M)

            anexo = AnexoExpediente(user=self.academico.user,
                                    fecha=datetime.datetime.now())
            anexo.archivo.name = 'expediente/%s/%s' % (self.academico.user.username, final_name)
            anexo.save()


class InvitadoMembresiaComite(models.Model):
    estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
    invitado = models.ForeignKey(Invitado,
                                 on_delete=models.CASCADE)

    year = models.PositiveSmallIntegerField("Año")
    semestre = models.PositiveSmallIntegerField(
        choices=((1, 1), (2, 2)))
    tipo = models.CharField(
        max_length=25,
        default='AED',
        choices=(
            ('AEM', 'Asesor externo maestría'),
            ('AED', 'Asesor Externo Doctorado'),
        ))

    class Meta:
        verbose_name_plural = "Invitaciones a Comités Tutorales"
        ordering = ['year', 'semestre' ]

    def __str__(self):
        return "%s : %s %s-%s: %s" % (self.estudiante,
                                  self.invitado,
                                  self.year,
                                  self.semestre,
                                  self.tipo)



tipos_membresia = (
    ('TPD', 'Tutor(a) principal de Doctorado'),
    ('CD', 'Cotutor(a) de Doctorado'),
    ('MCD', 'Miembro de comité de Doctorado'),

    ('TPM', 'Tutor(a) principal de Maestría'),
    ('CM', 'Cotutor(a) de Maestría'),
    ('MCM', 'Miembro de comité de Maestría'),
)

class MembresiaComite(models.Model):
    estudiante = models.ForeignKey(Estudiante,
                                   related_name="miembros_comite",
                                   on_delete=models.CASCADE)
    tutor = models.ForeignKey(Academico,
                              related_name="tutorias",
                              on_delete=models.CASCADE)

    year = models.PositiveSmallIntegerField("Año")
    semestre = models.PositiveSmallIntegerField(
        choices=((1, 1), (2, 2)))
    tipo = models.CharField(
        max_length=25,
        default='TPD',
        choices=tipos_membresia
    )

    ultima = models.BooleanField(default=False)
    
    def verifica_ultima(self):
        if self in self.estudiante.get_comite(plan=self.plan,
                                              con_invitados=False):
            return True
        else:
            return False

    def actualiza_ultima(self):
        self.ultima = self.verifica_ultima()
        self.save()
    
    @property
    def plan(self):
        if self.tipo.endswith('M'):
            return "Maestría"
        elif self.tipo.endswith('D'):
            return "Doctorado"
        else:
            return None

    class Meta:
        verbose_name_plural = "Membresías de Comités Tutorales"
        ordering = ['year', 'semestre' ]

    def __str__(self):
        return "[%s %s-%s] %s 🌱 %s" % (self.tipo,
                                        self.year,
                                        self.semestre,
                                        self.tutor,
                                        self.estudiante)




class Adscripcion(models.Model):
    perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE)
    institucion = models.ForeignKey(Institucion, on_delete=models.CASCADE)

    catedra_conacyt = models.BooleanField(default=False)
    nombramiento = models.CharField(max_length=50)
    anno_nombramiento = models.PositiveSmallIntegerField("año de nombramiento")

    class Meta:
        verbose_name_plural = "Adscripciones"

    def __str__(self):
        return f"{self.institucion}"



def curso_path(instance, filename):
    (root, ext) = os.path.splitext(filename)
    return os.path.join(u'cursos/%s/%s' % (instance.id,
                                           slugify(root) + ext))


class Asignatura(models.Model):
    asignatura = models.CharField(max_length=200)

    clave = models.CharField(max_length=20, blank=True, null=True)

    creditos = models.PositiveSmallIntegerField(default=0)

    horas = models.PositiveSmallIntegerField(default=0)

    campos_de_conocimiento = models.ManyToManyField(
        CampoConocimiento,
        blank=True)

    tipo = models.CharField(max_length=40,
                            choices=((u"Obligatoria",
                                      "Obligatoria"),
                                     (u"Obligatorias por campo",
                                      u"Obligatorias por campo"),
                                     (u"Optativa",
                                      u"Optativa"),
                                     (u"Seminario de Doctorado",
                                      u"Seminario de Doctorado")))

    estado = models.CharField(max_length=40,
                              default='propuesta',
                              choices=((u"propuesta",
                                        u"propuesta"),
                                       (u"rechazada",
                                        u"rechazada"),
                                       (u"aceptada",
                                        u"aceptada")))

    programa = models.FileField("Formato de curso nuevo completo.",
                                upload_to=curso_path,
                                blank=True, null=True)

    proponente = models.ForeignKey(User,
                                   on_delete=models.CASCADE, null=True, blank=True)


    notas = GenericRelation(Nota,
                            related_query_name='asignatura')


    class Meta:
        ordering = ['asignatura', 'clave', ]

    def programa_url(self):
        if self.programa:
            return self.programa.url
        else:
            return None


    def get_asignatura(self):
        return u'%s' % self.asignatura


    def __str__(self):
        return u'%s (%s)' % (self.asignatura,
                                self.tipo)


class Curso(models.Model):
    convocatoria = models.ForeignKey(ConvocatoriaCurso, on_delete=models.CASCADE, null=True)
    asignatura = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
    grupo = models.CharField(max_length=20, blank=True, null=True)
    year = models.PositiveSmallIntegerField("Año")
    semestre = models.PositiveSmallIntegerField(
        choices=((1, 1), (2, 2)))
    entidad = models.CharField(max_length=20,
                               blank=True, null=True,
                               choices=(('3', '3'),
                                        ('600', '600'),
                                        ('700', '700')))
    sede = models.CharField(max_length=80,
                            blank=True, null=True,
                            choices=(
                                (u'en línea', u'en línea'),
                                ('CDMX', 'CDMX'),
                                ('Morelia', 'Morelia'),
                                (u'León', u'León')))

    profesores = models.TextField("Profesores", blank=True, null=True)
    contacto = models.TextField("Contacto", blank=True, null=True)

    academicos = models.ManyToManyField(Academico, help_text="Profesores que impartirán el curso.")

    aula = models.CharField(max_length=80, blank=True, null=True)
    horario = models.CharField(max_length=300, blank=True, null=True, help_text="Día(s) y horarios.")

    intersemestral = models.BooleanField(default=False, help_text="marque si solicita impartir el curso en el intersemestre")

    status = models.CharField(max_length=10,
                              default='solicitado',
                              choices=[('solicitado', 'solicitado'),
                                       ('aceptado', 'aceptado'),
                                       ('rechazado', 'rechazado'),
                                       ('cancelado', 'cancelado'),
                                       ('publicado', 'publicado'),
                                       ('concluido', 'concluido'),])

    observaciones_profesores = models.TextField(blank=True, help_text="Observaciones de profesores")

    notas = GenericRelation(Nota,
                            related_query_name='curso')


    def genera_constancias(self):
        if self.status == 'concluido':


            outdir = '%s/cursos/%s/' % (MEDIA_ROOT, self.id)
            pathlib.Path(outdir).mkdir(parents=True, exist_ok=True)

            tmpname = 'cursoplain_profesores.pdf'

            with open(outdir + tmpname + '.md', 'w', encoding='utf-8') as f:
                f.write(render_to_string('posgradmin/constancia_curso_profesores.md',
                                         {'fecha': datetime.date.today(),
                                          'firma': BASE_DIR + '/docs/firma.png',
                                          'academicos': list(self.academicos.all()),
                                          'curso': self}))

            final_name = tmpname.replace('cursoplain', 'constancia_curso')

            os.chdir(outdir)
            subprocess.run(["pandoc",
                            outdir + tmpname + '.md',
                            "--pdf-engine=xelatex",
                            "--to",'latex',
                            "--output", outdir + tmpname])

            C = PdfReader(outdir + tmpname)
            M = PdfReader(BASE_DIR + '/docs/membrete_pcs.pdf')
            w = PdfWriter()
            merger = PageMerge(M.pages[0])
            merger.add(C.pages[0]).render()

            pathlib.Path(outdir + tmpname).unlink()
            pathlib.Path(outdir + tmpname + '.md').unlink()

            w.write(outdir + final_name, M)


    class Meta:
        verbose_name_plural = "Cursos"
        ordering = ['asignatura', ]

    def __str__(self):
        return u'%s, %s-%s' % (self.asignatura,
                               self.year,
                               self.semestre)



class Anomalia(models.Model):
    fecha = models.DateField()
    titulo = models.CharField(max_length=200)

    tipo = models.CharField(
        max_length=25,
        default='X',
        choices=(
            ('X', 'X'),
            ('no egresado', 'no egresado'),
            ))

    estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)

    notas = GenericRelation(Nota,
                            related_query_name='anomalia')

    def __str__(self):
        return u'%s <%s> %s' % (self.fecha,
                               self.tipo,
                               self.estudiante)
