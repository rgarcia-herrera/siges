from .models import EntidadPCS


def membresias_por_entidad(entidad):
    membresias = []
    for a in entidad.academicos_adscritos.all():
        for m in a.tutorias.all():
            if m.verifica_ultima():
                membresias.append(m)
    return membresias
    


def reporte(entidad):
    """
    membresía tipo
    estudiante cuenta
    estudiante nombre
    estudiante apellido
    estudiante correo
    estudiante generación
    estudiante estado
    tutor nombre
    tutor apellido
    tutor acreditacion
    tutor correo
    tutor adscripción pcs
    """
    return [[m.tipo,
             m.estudiante.cuenta,
             m.estudiante.user.first_name,
             m.estudiante.user.last_name,
             m.estudiante.user.email,
             m.estudiante.generacion(m.plan),
             m.estudiante.ultimo_estado_plan(m.plan),
             m.tutor.user.first_name,
             m.tutor.user.last_name,
             m.tutor.acreditacion,
             m.tutor.user.email,
             str(entidad)]
            for m in membresias_por_entidad(entidad)]

